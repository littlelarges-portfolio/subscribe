import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:subscribe_app/domain/subscription/subscription.dart';

part 'subscription_event.dart';
part 'subscription_state.dart';

part 'subscription_bloc.freezed.dart';

@injectable
class SubscriptionBloc extends Bloc<SubscriptionEvent, SubscriptionState> {
  SubscriptionBloc() : super(SubscriptionState.initial()) {
    on<SubscriptionEvent>((event, emit) {
      event.map(
        added: (e) {
          final subscriptions = state.subscriptions.plusElement(e.subscription);

          emit(
            state.copyWith(subscriptions: subscriptions),
          );
        },
        removed: (e) {
          final subscriptions =
              state.subscriptions.minusElement(e.subscription);

          emit(
            state.copyWith(subscriptions: subscriptions),
          );
        },
      );
    });
  }
}
