part of 'subscription_bloc.dart';

@freezed
class SubscriptionEvent with _$SubscriptionEvent {
  const factory SubscriptionEvent.added(Subscription subscription) = _Added;
  const factory SubscriptionEvent.removed(Subscription subscription) = _Removed;
}
