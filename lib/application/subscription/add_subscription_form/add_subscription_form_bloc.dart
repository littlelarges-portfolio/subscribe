import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:subscribe_app/domain/subscription/subscription.dart';
import 'package:subscribe_app/domain/subscription/value_objects.dart';

part 'add_subscription_form_event.dart';
part 'add_subscription_form_state.dart';
part 'add_subscription_form_bloc.freezed.dart';

@injectable
class AddSubscriptionFormBloc
    extends Bloc<AddSubscriptionFormEvent, AddSubscriptionFormState> {
  AddSubscriptionFormBloc() : super(AddSubscriptionFormState.initial()) {
    on<AddSubscriptionFormEvent>((event, emit) {
      event.map(
        nameChanged: (e) {
          emit(
            state.copyWith(
              name: Name(e.nameStr),
            ),
          );
        },
        costChanged: (e) {
          emit(
            state.copyWith(
              cost: Cost(e.costStr),
            ),
          );
        },
        siteUrlChanged: (e) {
          emit(
            state.copyWith(
              siteUrl: SiteUrl(e.siteUrlStr),
            ),
          );
        },
        noteChanged: (e) {
          emit(
            state.copyWith(
              note: e.noteStr,
            ),
          );
        },
        subscriptionDateChanged: (e) {
          emit(
            state.copyWith(
              subscriptionDate: SubscriptionDate(
                subscriptionDate: e.subscriptionDate,
                expirationDate: e.expirationDate,
              ),
              expirationDate: ExpirationDate(
                expirationDate: e.expirationDate,
                subscriptionDate: e.subscriptionDate,
              ),
            ),
          );
        },
        expirationDateChanged: (e) {
          emit(
            state.copyWith(
              expirationDate: ExpirationDate(
                expirationDate: e.expirationDate,
                subscriptionDate: e.subscriptionDate,
              ),
              subscriptionDate: SubscriptionDate(
                subscriptionDate: e.subscriptionDate,
                expirationDate: e.expirationDate,
              ),
            ),
          );
        },
        sendMeNotificationsPressed: (e) {
          final isNameValid = state.name.isValid();
          final isCostValid = state.cost.isValid();
          final isSiteUrlValid = state.siteUrl.isValid();
          final isSubscriptionDateValid = state.subscriptionDate.isValid();
          final isExpirationDateValid = state.expirationDate.isValid();

          if (isNameValid &&
              isCostValid &&
              isSiteUrlValid &&
              isSubscriptionDateValid &&
              isExpirationDateValid) {
            emit(
              state.copyWith(
                validatedSubscription: Subscription(
                  name: state.name,
                  cost: state.cost,
                  siteUrl: state.siteUrl,
                  note: state.note,
                  subscriptionDate: state.subscriptionDate,
                  expirationDate: state.expirationDate,
                  iconPath: state.iconPath,
                ),
              ),
            );
          }

          emit(
            state.copyWith(
              showErrorMessages: AutovalidateMode.always,
            ),
          );
        },
        iconPathChanged: (e) {
          emit(
            state.copyWith(iconPath: e.iconPath),
          );
        },
      );
    });
  }
}
