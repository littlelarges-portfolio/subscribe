part of 'add_subscription_form_bloc.dart';

@freezed
class AddSubscriptionFormEvent with _$AddSubscriptionFormEvent {
  const factory AddSubscriptionFormEvent.nameChanged(String nameStr) =
      _NameChanged;
  const factory AddSubscriptionFormEvent.costChanged(String costStr) =
      _CostChanged;
  const factory AddSubscriptionFormEvent.siteUrlChanged(String siteUrlStr) =
      _SiteUrlChanged;
  const factory AddSubscriptionFormEvent.noteChanged(String noteStr) =
      _NoteChanged;
  const factory AddSubscriptionFormEvent.subscriptionDateChanged({
    required DateTime subscriptionDate,
    required DateTime expirationDate,
  }) = _SubscriptionDateChanged;
  const factory AddSubscriptionFormEvent.expirationDateChanged({
    required DateTime expirationDate,
    required DateTime subscriptionDate,
  }) = _ExpirationDateChanged;
  const factory AddSubscriptionFormEvent.iconPathChanged(
    String? iconPath,
  ) = _IconPathChanged;

  const factory AddSubscriptionFormEvent.sendMeNotificationsPressed() =
      _SendMeNotificationsPressed;
}
