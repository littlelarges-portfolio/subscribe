part of 'add_subscription_form_bloc.dart';

@freezed
class AddSubscriptionFormState with _$AddSubscriptionFormState {
  const factory AddSubscriptionFormState({
    required Name name,
    required Cost cost,
    required SiteUrl siteUrl,
    required String note,
    required SubscriptionDate subscriptionDate,
    required ExpirationDate expirationDate,
    required AutovalidateMode showErrorMessages,
    required Subscription? validatedSubscription,
    String? iconPath,
  }) = _AddSubscriptionFormState;

  factory AddSubscriptionFormState.initial() => AddSubscriptionFormState(
        name: Name(''),
        cost: Cost(''),
        siteUrl: SiteUrl(''),
        note: '',
        showErrorMessages: AutovalidateMode.disabled,
        subscriptionDate: SubscriptionDate(
            subscriptionDate: DateTime(0), expirationDate: DateTime(0),),
        expirationDate: ExpirationDate(
            expirationDate: DateTime(0), subscriptionDate: DateTime(0),),
        validatedSubscription: null,
      );
}
