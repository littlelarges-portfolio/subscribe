// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'add_subscription_form_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$AddSubscriptionFormEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String nameStr) nameChanged,
    required TResult Function(String costStr) costChanged,
    required TResult Function(String siteUrlStr) siteUrlChanged,
    required TResult Function(String noteStr) noteChanged,
    required TResult Function(
            DateTime subscriptionDate, DateTime expirationDate)
        subscriptionDateChanged,
    required TResult Function(
            DateTime expirationDate, DateTime subscriptionDate)
        expirationDateChanged,
    required TResult Function(String? iconPath) iconPathChanged,
    required TResult Function() sendMeNotificationsPressed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String nameStr)? nameChanged,
    TResult? Function(String costStr)? costChanged,
    TResult? Function(String siteUrlStr)? siteUrlChanged,
    TResult? Function(String noteStr)? noteChanged,
    TResult? Function(DateTime subscriptionDate, DateTime expirationDate)?
        subscriptionDateChanged,
    TResult? Function(DateTime expirationDate, DateTime subscriptionDate)?
        expirationDateChanged,
    TResult? Function(String? iconPath)? iconPathChanged,
    TResult? Function()? sendMeNotificationsPressed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String nameStr)? nameChanged,
    TResult Function(String costStr)? costChanged,
    TResult Function(String siteUrlStr)? siteUrlChanged,
    TResult Function(String noteStr)? noteChanged,
    TResult Function(DateTime subscriptionDate, DateTime expirationDate)?
        subscriptionDateChanged,
    TResult Function(DateTime expirationDate, DateTime subscriptionDate)?
        expirationDateChanged,
    TResult Function(String? iconPath)? iconPathChanged,
    TResult Function()? sendMeNotificationsPressed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NameChanged value) nameChanged,
    required TResult Function(_CostChanged value) costChanged,
    required TResult Function(_SiteUrlChanged value) siteUrlChanged,
    required TResult Function(_NoteChanged value) noteChanged,
    required TResult Function(_SubscriptionDateChanged value)
        subscriptionDateChanged,
    required TResult Function(_ExpirationDateChanged value)
        expirationDateChanged,
    required TResult Function(_IconPathChanged value) iconPathChanged,
    required TResult Function(_SendMeNotificationsPressed value)
        sendMeNotificationsPressed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NameChanged value)? nameChanged,
    TResult? Function(_CostChanged value)? costChanged,
    TResult? Function(_SiteUrlChanged value)? siteUrlChanged,
    TResult? Function(_NoteChanged value)? noteChanged,
    TResult? Function(_SubscriptionDateChanged value)? subscriptionDateChanged,
    TResult? Function(_ExpirationDateChanged value)? expirationDateChanged,
    TResult? Function(_IconPathChanged value)? iconPathChanged,
    TResult? Function(_SendMeNotificationsPressed value)?
        sendMeNotificationsPressed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NameChanged value)? nameChanged,
    TResult Function(_CostChanged value)? costChanged,
    TResult Function(_SiteUrlChanged value)? siteUrlChanged,
    TResult Function(_NoteChanged value)? noteChanged,
    TResult Function(_SubscriptionDateChanged value)? subscriptionDateChanged,
    TResult Function(_ExpirationDateChanged value)? expirationDateChanged,
    TResult Function(_IconPathChanged value)? iconPathChanged,
    TResult Function(_SendMeNotificationsPressed value)?
        sendMeNotificationsPressed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AddSubscriptionFormEventCopyWith<$Res> {
  factory $AddSubscriptionFormEventCopyWith(AddSubscriptionFormEvent value,
          $Res Function(AddSubscriptionFormEvent) then) =
      _$AddSubscriptionFormEventCopyWithImpl<$Res, AddSubscriptionFormEvent>;
}

/// @nodoc
class _$AddSubscriptionFormEventCopyWithImpl<$Res,
        $Val extends AddSubscriptionFormEvent>
    implements $AddSubscriptionFormEventCopyWith<$Res> {
  _$AddSubscriptionFormEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$NameChangedImplCopyWith<$Res> {
  factory _$$NameChangedImplCopyWith(
          _$NameChangedImpl value, $Res Function(_$NameChangedImpl) then) =
      __$$NameChangedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String nameStr});
}

/// @nodoc
class __$$NameChangedImplCopyWithImpl<$Res>
    extends _$AddSubscriptionFormEventCopyWithImpl<$Res, _$NameChangedImpl>
    implements _$$NameChangedImplCopyWith<$Res> {
  __$$NameChangedImplCopyWithImpl(
      _$NameChangedImpl _value, $Res Function(_$NameChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? nameStr = null,
  }) {
    return _then(_$NameChangedImpl(
      null == nameStr
          ? _value.nameStr
          : nameStr // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$NameChangedImpl implements _NameChanged {
  const _$NameChangedImpl(this.nameStr);

  @override
  final String nameStr;

  @override
  String toString() {
    return 'AddSubscriptionFormEvent.nameChanged(nameStr: $nameStr)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$NameChangedImpl &&
            (identical(other.nameStr, nameStr) || other.nameStr == nameStr));
  }

  @override
  int get hashCode => Object.hash(runtimeType, nameStr);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$NameChangedImplCopyWith<_$NameChangedImpl> get copyWith =>
      __$$NameChangedImplCopyWithImpl<_$NameChangedImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String nameStr) nameChanged,
    required TResult Function(String costStr) costChanged,
    required TResult Function(String siteUrlStr) siteUrlChanged,
    required TResult Function(String noteStr) noteChanged,
    required TResult Function(
            DateTime subscriptionDate, DateTime expirationDate)
        subscriptionDateChanged,
    required TResult Function(
            DateTime expirationDate, DateTime subscriptionDate)
        expirationDateChanged,
    required TResult Function(String? iconPath) iconPathChanged,
    required TResult Function() sendMeNotificationsPressed,
  }) {
    return nameChanged(nameStr);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String nameStr)? nameChanged,
    TResult? Function(String costStr)? costChanged,
    TResult? Function(String siteUrlStr)? siteUrlChanged,
    TResult? Function(String noteStr)? noteChanged,
    TResult? Function(DateTime subscriptionDate, DateTime expirationDate)?
        subscriptionDateChanged,
    TResult? Function(DateTime expirationDate, DateTime subscriptionDate)?
        expirationDateChanged,
    TResult? Function(String? iconPath)? iconPathChanged,
    TResult? Function()? sendMeNotificationsPressed,
  }) {
    return nameChanged?.call(nameStr);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String nameStr)? nameChanged,
    TResult Function(String costStr)? costChanged,
    TResult Function(String siteUrlStr)? siteUrlChanged,
    TResult Function(String noteStr)? noteChanged,
    TResult Function(DateTime subscriptionDate, DateTime expirationDate)?
        subscriptionDateChanged,
    TResult Function(DateTime expirationDate, DateTime subscriptionDate)?
        expirationDateChanged,
    TResult Function(String? iconPath)? iconPathChanged,
    TResult Function()? sendMeNotificationsPressed,
    required TResult orElse(),
  }) {
    if (nameChanged != null) {
      return nameChanged(nameStr);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NameChanged value) nameChanged,
    required TResult Function(_CostChanged value) costChanged,
    required TResult Function(_SiteUrlChanged value) siteUrlChanged,
    required TResult Function(_NoteChanged value) noteChanged,
    required TResult Function(_SubscriptionDateChanged value)
        subscriptionDateChanged,
    required TResult Function(_ExpirationDateChanged value)
        expirationDateChanged,
    required TResult Function(_IconPathChanged value) iconPathChanged,
    required TResult Function(_SendMeNotificationsPressed value)
        sendMeNotificationsPressed,
  }) {
    return nameChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NameChanged value)? nameChanged,
    TResult? Function(_CostChanged value)? costChanged,
    TResult? Function(_SiteUrlChanged value)? siteUrlChanged,
    TResult? Function(_NoteChanged value)? noteChanged,
    TResult? Function(_SubscriptionDateChanged value)? subscriptionDateChanged,
    TResult? Function(_ExpirationDateChanged value)? expirationDateChanged,
    TResult? Function(_IconPathChanged value)? iconPathChanged,
    TResult? Function(_SendMeNotificationsPressed value)?
        sendMeNotificationsPressed,
  }) {
    return nameChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NameChanged value)? nameChanged,
    TResult Function(_CostChanged value)? costChanged,
    TResult Function(_SiteUrlChanged value)? siteUrlChanged,
    TResult Function(_NoteChanged value)? noteChanged,
    TResult Function(_SubscriptionDateChanged value)? subscriptionDateChanged,
    TResult Function(_ExpirationDateChanged value)? expirationDateChanged,
    TResult Function(_IconPathChanged value)? iconPathChanged,
    TResult Function(_SendMeNotificationsPressed value)?
        sendMeNotificationsPressed,
    required TResult orElse(),
  }) {
    if (nameChanged != null) {
      return nameChanged(this);
    }
    return orElse();
  }
}

abstract class _NameChanged implements AddSubscriptionFormEvent {
  const factory _NameChanged(final String nameStr) = _$NameChangedImpl;

  String get nameStr;
  @JsonKey(ignore: true)
  _$$NameChangedImplCopyWith<_$NameChangedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$CostChangedImplCopyWith<$Res> {
  factory _$$CostChangedImplCopyWith(
          _$CostChangedImpl value, $Res Function(_$CostChangedImpl) then) =
      __$$CostChangedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String costStr});
}

/// @nodoc
class __$$CostChangedImplCopyWithImpl<$Res>
    extends _$AddSubscriptionFormEventCopyWithImpl<$Res, _$CostChangedImpl>
    implements _$$CostChangedImplCopyWith<$Res> {
  __$$CostChangedImplCopyWithImpl(
      _$CostChangedImpl _value, $Res Function(_$CostChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? costStr = null,
  }) {
    return _then(_$CostChangedImpl(
      null == costStr
          ? _value.costStr
          : costStr // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$CostChangedImpl implements _CostChanged {
  const _$CostChangedImpl(this.costStr);

  @override
  final String costStr;

  @override
  String toString() {
    return 'AddSubscriptionFormEvent.costChanged(costStr: $costStr)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CostChangedImpl &&
            (identical(other.costStr, costStr) || other.costStr == costStr));
  }

  @override
  int get hashCode => Object.hash(runtimeType, costStr);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CostChangedImplCopyWith<_$CostChangedImpl> get copyWith =>
      __$$CostChangedImplCopyWithImpl<_$CostChangedImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String nameStr) nameChanged,
    required TResult Function(String costStr) costChanged,
    required TResult Function(String siteUrlStr) siteUrlChanged,
    required TResult Function(String noteStr) noteChanged,
    required TResult Function(
            DateTime subscriptionDate, DateTime expirationDate)
        subscriptionDateChanged,
    required TResult Function(
            DateTime expirationDate, DateTime subscriptionDate)
        expirationDateChanged,
    required TResult Function(String? iconPath) iconPathChanged,
    required TResult Function() sendMeNotificationsPressed,
  }) {
    return costChanged(costStr);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String nameStr)? nameChanged,
    TResult? Function(String costStr)? costChanged,
    TResult? Function(String siteUrlStr)? siteUrlChanged,
    TResult? Function(String noteStr)? noteChanged,
    TResult? Function(DateTime subscriptionDate, DateTime expirationDate)?
        subscriptionDateChanged,
    TResult? Function(DateTime expirationDate, DateTime subscriptionDate)?
        expirationDateChanged,
    TResult? Function(String? iconPath)? iconPathChanged,
    TResult? Function()? sendMeNotificationsPressed,
  }) {
    return costChanged?.call(costStr);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String nameStr)? nameChanged,
    TResult Function(String costStr)? costChanged,
    TResult Function(String siteUrlStr)? siteUrlChanged,
    TResult Function(String noteStr)? noteChanged,
    TResult Function(DateTime subscriptionDate, DateTime expirationDate)?
        subscriptionDateChanged,
    TResult Function(DateTime expirationDate, DateTime subscriptionDate)?
        expirationDateChanged,
    TResult Function(String? iconPath)? iconPathChanged,
    TResult Function()? sendMeNotificationsPressed,
    required TResult orElse(),
  }) {
    if (costChanged != null) {
      return costChanged(costStr);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NameChanged value) nameChanged,
    required TResult Function(_CostChanged value) costChanged,
    required TResult Function(_SiteUrlChanged value) siteUrlChanged,
    required TResult Function(_NoteChanged value) noteChanged,
    required TResult Function(_SubscriptionDateChanged value)
        subscriptionDateChanged,
    required TResult Function(_ExpirationDateChanged value)
        expirationDateChanged,
    required TResult Function(_IconPathChanged value) iconPathChanged,
    required TResult Function(_SendMeNotificationsPressed value)
        sendMeNotificationsPressed,
  }) {
    return costChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NameChanged value)? nameChanged,
    TResult? Function(_CostChanged value)? costChanged,
    TResult? Function(_SiteUrlChanged value)? siteUrlChanged,
    TResult? Function(_NoteChanged value)? noteChanged,
    TResult? Function(_SubscriptionDateChanged value)? subscriptionDateChanged,
    TResult? Function(_ExpirationDateChanged value)? expirationDateChanged,
    TResult? Function(_IconPathChanged value)? iconPathChanged,
    TResult? Function(_SendMeNotificationsPressed value)?
        sendMeNotificationsPressed,
  }) {
    return costChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NameChanged value)? nameChanged,
    TResult Function(_CostChanged value)? costChanged,
    TResult Function(_SiteUrlChanged value)? siteUrlChanged,
    TResult Function(_NoteChanged value)? noteChanged,
    TResult Function(_SubscriptionDateChanged value)? subscriptionDateChanged,
    TResult Function(_ExpirationDateChanged value)? expirationDateChanged,
    TResult Function(_IconPathChanged value)? iconPathChanged,
    TResult Function(_SendMeNotificationsPressed value)?
        sendMeNotificationsPressed,
    required TResult orElse(),
  }) {
    if (costChanged != null) {
      return costChanged(this);
    }
    return orElse();
  }
}

abstract class _CostChanged implements AddSubscriptionFormEvent {
  const factory _CostChanged(final String costStr) = _$CostChangedImpl;

  String get costStr;
  @JsonKey(ignore: true)
  _$$CostChangedImplCopyWith<_$CostChangedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SiteUrlChangedImplCopyWith<$Res> {
  factory _$$SiteUrlChangedImplCopyWith(_$SiteUrlChangedImpl value,
          $Res Function(_$SiteUrlChangedImpl) then) =
      __$$SiteUrlChangedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String siteUrlStr});
}

/// @nodoc
class __$$SiteUrlChangedImplCopyWithImpl<$Res>
    extends _$AddSubscriptionFormEventCopyWithImpl<$Res, _$SiteUrlChangedImpl>
    implements _$$SiteUrlChangedImplCopyWith<$Res> {
  __$$SiteUrlChangedImplCopyWithImpl(
      _$SiteUrlChangedImpl _value, $Res Function(_$SiteUrlChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? siteUrlStr = null,
  }) {
    return _then(_$SiteUrlChangedImpl(
      null == siteUrlStr
          ? _value.siteUrlStr
          : siteUrlStr // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$SiteUrlChangedImpl implements _SiteUrlChanged {
  const _$SiteUrlChangedImpl(this.siteUrlStr);

  @override
  final String siteUrlStr;

  @override
  String toString() {
    return 'AddSubscriptionFormEvent.siteUrlChanged(siteUrlStr: $siteUrlStr)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SiteUrlChangedImpl &&
            (identical(other.siteUrlStr, siteUrlStr) ||
                other.siteUrlStr == siteUrlStr));
  }

  @override
  int get hashCode => Object.hash(runtimeType, siteUrlStr);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SiteUrlChangedImplCopyWith<_$SiteUrlChangedImpl> get copyWith =>
      __$$SiteUrlChangedImplCopyWithImpl<_$SiteUrlChangedImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String nameStr) nameChanged,
    required TResult Function(String costStr) costChanged,
    required TResult Function(String siteUrlStr) siteUrlChanged,
    required TResult Function(String noteStr) noteChanged,
    required TResult Function(
            DateTime subscriptionDate, DateTime expirationDate)
        subscriptionDateChanged,
    required TResult Function(
            DateTime expirationDate, DateTime subscriptionDate)
        expirationDateChanged,
    required TResult Function(String? iconPath) iconPathChanged,
    required TResult Function() sendMeNotificationsPressed,
  }) {
    return siteUrlChanged(siteUrlStr);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String nameStr)? nameChanged,
    TResult? Function(String costStr)? costChanged,
    TResult? Function(String siteUrlStr)? siteUrlChanged,
    TResult? Function(String noteStr)? noteChanged,
    TResult? Function(DateTime subscriptionDate, DateTime expirationDate)?
        subscriptionDateChanged,
    TResult? Function(DateTime expirationDate, DateTime subscriptionDate)?
        expirationDateChanged,
    TResult? Function(String? iconPath)? iconPathChanged,
    TResult? Function()? sendMeNotificationsPressed,
  }) {
    return siteUrlChanged?.call(siteUrlStr);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String nameStr)? nameChanged,
    TResult Function(String costStr)? costChanged,
    TResult Function(String siteUrlStr)? siteUrlChanged,
    TResult Function(String noteStr)? noteChanged,
    TResult Function(DateTime subscriptionDate, DateTime expirationDate)?
        subscriptionDateChanged,
    TResult Function(DateTime expirationDate, DateTime subscriptionDate)?
        expirationDateChanged,
    TResult Function(String? iconPath)? iconPathChanged,
    TResult Function()? sendMeNotificationsPressed,
    required TResult orElse(),
  }) {
    if (siteUrlChanged != null) {
      return siteUrlChanged(siteUrlStr);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NameChanged value) nameChanged,
    required TResult Function(_CostChanged value) costChanged,
    required TResult Function(_SiteUrlChanged value) siteUrlChanged,
    required TResult Function(_NoteChanged value) noteChanged,
    required TResult Function(_SubscriptionDateChanged value)
        subscriptionDateChanged,
    required TResult Function(_ExpirationDateChanged value)
        expirationDateChanged,
    required TResult Function(_IconPathChanged value) iconPathChanged,
    required TResult Function(_SendMeNotificationsPressed value)
        sendMeNotificationsPressed,
  }) {
    return siteUrlChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NameChanged value)? nameChanged,
    TResult? Function(_CostChanged value)? costChanged,
    TResult? Function(_SiteUrlChanged value)? siteUrlChanged,
    TResult? Function(_NoteChanged value)? noteChanged,
    TResult? Function(_SubscriptionDateChanged value)? subscriptionDateChanged,
    TResult? Function(_ExpirationDateChanged value)? expirationDateChanged,
    TResult? Function(_IconPathChanged value)? iconPathChanged,
    TResult? Function(_SendMeNotificationsPressed value)?
        sendMeNotificationsPressed,
  }) {
    return siteUrlChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NameChanged value)? nameChanged,
    TResult Function(_CostChanged value)? costChanged,
    TResult Function(_SiteUrlChanged value)? siteUrlChanged,
    TResult Function(_NoteChanged value)? noteChanged,
    TResult Function(_SubscriptionDateChanged value)? subscriptionDateChanged,
    TResult Function(_ExpirationDateChanged value)? expirationDateChanged,
    TResult Function(_IconPathChanged value)? iconPathChanged,
    TResult Function(_SendMeNotificationsPressed value)?
        sendMeNotificationsPressed,
    required TResult orElse(),
  }) {
    if (siteUrlChanged != null) {
      return siteUrlChanged(this);
    }
    return orElse();
  }
}

abstract class _SiteUrlChanged implements AddSubscriptionFormEvent {
  const factory _SiteUrlChanged(final String siteUrlStr) = _$SiteUrlChangedImpl;

  String get siteUrlStr;
  @JsonKey(ignore: true)
  _$$SiteUrlChangedImplCopyWith<_$SiteUrlChangedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$NoteChangedImplCopyWith<$Res> {
  factory _$$NoteChangedImplCopyWith(
          _$NoteChangedImpl value, $Res Function(_$NoteChangedImpl) then) =
      __$$NoteChangedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String noteStr});
}

/// @nodoc
class __$$NoteChangedImplCopyWithImpl<$Res>
    extends _$AddSubscriptionFormEventCopyWithImpl<$Res, _$NoteChangedImpl>
    implements _$$NoteChangedImplCopyWith<$Res> {
  __$$NoteChangedImplCopyWithImpl(
      _$NoteChangedImpl _value, $Res Function(_$NoteChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? noteStr = null,
  }) {
    return _then(_$NoteChangedImpl(
      null == noteStr
          ? _value.noteStr
          : noteStr // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$NoteChangedImpl implements _NoteChanged {
  const _$NoteChangedImpl(this.noteStr);

  @override
  final String noteStr;

  @override
  String toString() {
    return 'AddSubscriptionFormEvent.noteChanged(noteStr: $noteStr)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$NoteChangedImpl &&
            (identical(other.noteStr, noteStr) || other.noteStr == noteStr));
  }

  @override
  int get hashCode => Object.hash(runtimeType, noteStr);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$NoteChangedImplCopyWith<_$NoteChangedImpl> get copyWith =>
      __$$NoteChangedImplCopyWithImpl<_$NoteChangedImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String nameStr) nameChanged,
    required TResult Function(String costStr) costChanged,
    required TResult Function(String siteUrlStr) siteUrlChanged,
    required TResult Function(String noteStr) noteChanged,
    required TResult Function(
            DateTime subscriptionDate, DateTime expirationDate)
        subscriptionDateChanged,
    required TResult Function(
            DateTime expirationDate, DateTime subscriptionDate)
        expirationDateChanged,
    required TResult Function(String? iconPath) iconPathChanged,
    required TResult Function() sendMeNotificationsPressed,
  }) {
    return noteChanged(noteStr);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String nameStr)? nameChanged,
    TResult? Function(String costStr)? costChanged,
    TResult? Function(String siteUrlStr)? siteUrlChanged,
    TResult? Function(String noteStr)? noteChanged,
    TResult? Function(DateTime subscriptionDate, DateTime expirationDate)?
        subscriptionDateChanged,
    TResult? Function(DateTime expirationDate, DateTime subscriptionDate)?
        expirationDateChanged,
    TResult? Function(String? iconPath)? iconPathChanged,
    TResult? Function()? sendMeNotificationsPressed,
  }) {
    return noteChanged?.call(noteStr);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String nameStr)? nameChanged,
    TResult Function(String costStr)? costChanged,
    TResult Function(String siteUrlStr)? siteUrlChanged,
    TResult Function(String noteStr)? noteChanged,
    TResult Function(DateTime subscriptionDate, DateTime expirationDate)?
        subscriptionDateChanged,
    TResult Function(DateTime expirationDate, DateTime subscriptionDate)?
        expirationDateChanged,
    TResult Function(String? iconPath)? iconPathChanged,
    TResult Function()? sendMeNotificationsPressed,
    required TResult orElse(),
  }) {
    if (noteChanged != null) {
      return noteChanged(noteStr);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NameChanged value) nameChanged,
    required TResult Function(_CostChanged value) costChanged,
    required TResult Function(_SiteUrlChanged value) siteUrlChanged,
    required TResult Function(_NoteChanged value) noteChanged,
    required TResult Function(_SubscriptionDateChanged value)
        subscriptionDateChanged,
    required TResult Function(_ExpirationDateChanged value)
        expirationDateChanged,
    required TResult Function(_IconPathChanged value) iconPathChanged,
    required TResult Function(_SendMeNotificationsPressed value)
        sendMeNotificationsPressed,
  }) {
    return noteChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NameChanged value)? nameChanged,
    TResult? Function(_CostChanged value)? costChanged,
    TResult? Function(_SiteUrlChanged value)? siteUrlChanged,
    TResult? Function(_NoteChanged value)? noteChanged,
    TResult? Function(_SubscriptionDateChanged value)? subscriptionDateChanged,
    TResult? Function(_ExpirationDateChanged value)? expirationDateChanged,
    TResult? Function(_IconPathChanged value)? iconPathChanged,
    TResult? Function(_SendMeNotificationsPressed value)?
        sendMeNotificationsPressed,
  }) {
    return noteChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NameChanged value)? nameChanged,
    TResult Function(_CostChanged value)? costChanged,
    TResult Function(_SiteUrlChanged value)? siteUrlChanged,
    TResult Function(_NoteChanged value)? noteChanged,
    TResult Function(_SubscriptionDateChanged value)? subscriptionDateChanged,
    TResult Function(_ExpirationDateChanged value)? expirationDateChanged,
    TResult Function(_IconPathChanged value)? iconPathChanged,
    TResult Function(_SendMeNotificationsPressed value)?
        sendMeNotificationsPressed,
    required TResult orElse(),
  }) {
    if (noteChanged != null) {
      return noteChanged(this);
    }
    return orElse();
  }
}

abstract class _NoteChanged implements AddSubscriptionFormEvent {
  const factory _NoteChanged(final String noteStr) = _$NoteChangedImpl;

  String get noteStr;
  @JsonKey(ignore: true)
  _$$NoteChangedImplCopyWith<_$NoteChangedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SubscriptionDateChangedImplCopyWith<$Res> {
  factory _$$SubscriptionDateChangedImplCopyWith(
          _$SubscriptionDateChangedImpl value,
          $Res Function(_$SubscriptionDateChangedImpl) then) =
      __$$SubscriptionDateChangedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({DateTime subscriptionDate, DateTime expirationDate});
}

/// @nodoc
class __$$SubscriptionDateChangedImplCopyWithImpl<$Res>
    extends _$AddSubscriptionFormEventCopyWithImpl<$Res,
        _$SubscriptionDateChangedImpl>
    implements _$$SubscriptionDateChangedImplCopyWith<$Res> {
  __$$SubscriptionDateChangedImplCopyWithImpl(
      _$SubscriptionDateChangedImpl _value,
      $Res Function(_$SubscriptionDateChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? subscriptionDate = null,
    Object? expirationDate = null,
  }) {
    return _then(_$SubscriptionDateChangedImpl(
      subscriptionDate: null == subscriptionDate
          ? _value.subscriptionDate
          : subscriptionDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      expirationDate: null == expirationDate
          ? _value.expirationDate
          : expirationDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc

class _$SubscriptionDateChangedImpl implements _SubscriptionDateChanged {
  const _$SubscriptionDateChangedImpl(
      {required this.subscriptionDate, required this.expirationDate});

  @override
  final DateTime subscriptionDate;
  @override
  final DateTime expirationDate;

  @override
  String toString() {
    return 'AddSubscriptionFormEvent.subscriptionDateChanged(subscriptionDate: $subscriptionDate, expirationDate: $expirationDate)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SubscriptionDateChangedImpl &&
            (identical(other.subscriptionDate, subscriptionDate) ||
                other.subscriptionDate == subscriptionDate) &&
            (identical(other.expirationDate, expirationDate) ||
                other.expirationDate == expirationDate));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, subscriptionDate, expirationDate);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SubscriptionDateChangedImplCopyWith<_$SubscriptionDateChangedImpl>
      get copyWith => __$$SubscriptionDateChangedImplCopyWithImpl<
          _$SubscriptionDateChangedImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String nameStr) nameChanged,
    required TResult Function(String costStr) costChanged,
    required TResult Function(String siteUrlStr) siteUrlChanged,
    required TResult Function(String noteStr) noteChanged,
    required TResult Function(
            DateTime subscriptionDate, DateTime expirationDate)
        subscriptionDateChanged,
    required TResult Function(
            DateTime expirationDate, DateTime subscriptionDate)
        expirationDateChanged,
    required TResult Function(String? iconPath) iconPathChanged,
    required TResult Function() sendMeNotificationsPressed,
  }) {
    return subscriptionDateChanged(subscriptionDate, expirationDate);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String nameStr)? nameChanged,
    TResult? Function(String costStr)? costChanged,
    TResult? Function(String siteUrlStr)? siteUrlChanged,
    TResult? Function(String noteStr)? noteChanged,
    TResult? Function(DateTime subscriptionDate, DateTime expirationDate)?
        subscriptionDateChanged,
    TResult? Function(DateTime expirationDate, DateTime subscriptionDate)?
        expirationDateChanged,
    TResult? Function(String? iconPath)? iconPathChanged,
    TResult? Function()? sendMeNotificationsPressed,
  }) {
    return subscriptionDateChanged?.call(subscriptionDate, expirationDate);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String nameStr)? nameChanged,
    TResult Function(String costStr)? costChanged,
    TResult Function(String siteUrlStr)? siteUrlChanged,
    TResult Function(String noteStr)? noteChanged,
    TResult Function(DateTime subscriptionDate, DateTime expirationDate)?
        subscriptionDateChanged,
    TResult Function(DateTime expirationDate, DateTime subscriptionDate)?
        expirationDateChanged,
    TResult Function(String? iconPath)? iconPathChanged,
    TResult Function()? sendMeNotificationsPressed,
    required TResult orElse(),
  }) {
    if (subscriptionDateChanged != null) {
      return subscriptionDateChanged(subscriptionDate, expirationDate);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NameChanged value) nameChanged,
    required TResult Function(_CostChanged value) costChanged,
    required TResult Function(_SiteUrlChanged value) siteUrlChanged,
    required TResult Function(_NoteChanged value) noteChanged,
    required TResult Function(_SubscriptionDateChanged value)
        subscriptionDateChanged,
    required TResult Function(_ExpirationDateChanged value)
        expirationDateChanged,
    required TResult Function(_IconPathChanged value) iconPathChanged,
    required TResult Function(_SendMeNotificationsPressed value)
        sendMeNotificationsPressed,
  }) {
    return subscriptionDateChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NameChanged value)? nameChanged,
    TResult? Function(_CostChanged value)? costChanged,
    TResult? Function(_SiteUrlChanged value)? siteUrlChanged,
    TResult? Function(_NoteChanged value)? noteChanged,
    TResult? Function(_SubscriptionDateChanged value)? subscriptionDateChanged,
    TResult? Function(_ExpirationDateChanged value)? expirationDateChanged,
    TResult? Function(_IconPathChanged value)? iconPathChanged,
    TResult? Function(_SendMeNotificationsPressed value)?
        sendMeNotificationsPressed,
  }) {
    return subscriptionDateChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NameChanged value)? nameChanged,
    TResult Function(_CostChanged value)? costChanged,
    TResult Function(_SiteUrlChanged value)? siteUrlChanged,
    TResult Function(_NoteChanged value)? noteChanged,
    TResult Function(_SubscriptionDateChanged value)? subscriptionDateChanged,
    TResult Function(_ExpirationDateChanged value)? expirationDateChanged,
    TResult Function(_IconPathChanged value)? iconPathChanged,
    TResult Function(_SendMeNotificationsPressed value)?
        sendMeNotificationsPressed,
    required TResult orElse(),
  }) {
    if (subscriptionDateChanged != null) {
      return subscriptionDateChanged(this);
    }
    return orElse();
  }
}

abstract class _SubscriptionDateChanged implements AddSubscriptionFormEvent {
  const factory _SubscriptionDateChanged(
      {required final DateTime subscriptionDate,
      required final DateTime expirationDate}) = _$SubscriptionDateChangedImpl;

  DateTime get subscriptionDate;
  DateTime get expirationDate;
  @JsonKey(ignore: true)
  _$$SubscriptionDateChangedImplCopyWith<_$SubscriptionDateChangedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ExpirationDateChangedImplCopyWith<$Res> {
  factory _$$ExpirationDateChangedImplCopyWith(
          _$ExpirationDateChangedImpl value,
          $Res Function(_$ExpirationDateChangedImpl) then) =
      __$$ExpirationDateChangedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({DateTime expirationDate, DateTime subscriptionDate});
}

/// @nodoc
class __$$ExpirationDateChangedImplCopyWithImpl<$Res>
    extends _$AddSubscriptionFormEventCopyWithImpl<$Res,
        _$ExpirationDateChangedImpl>
    implements _$$ExpirationDateChangedImplCopyWith<$Res> {
  __$$ExpirationDateChangedImplCopyWithImpl(_$ExpirationDateChangedImpl _value,
      $Res Function(_$ExpirationDateChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? expirationDate = null,
    Object? subscriptionDate = null,
  }) {
    return _then(_$ExpirationDateChangedImpl(
      expirationDate: null == expirationDate
          ? _value.expirationDate
          : expirationDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      subscriptionDate: null == subscriptionDate
          ? _value.subscriptionDate
          : subscriptionDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc

class _$ExpirationDateChangedImpl implements _ExpirationDateChanged {
  const _$ExpirationDateChangedImpl(
      {required this.expirationDate, required this.subscriptionDate});

  @override
  final DateTime expirationDate;
  @override
  final DateTime subscriptionDate;

  @override
  String toString() {
    return 'AddSubscriptionFormEvent.expirationDateChanged(expirationDate: $expirationDate, subscriptionDate: $subscriptionDate)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ExpirationDateChangedImpl &&
            (identical(other.expirationDate, expirationDate) ||
                other.expirationDate == expirationDate) &&
            (identical(other.subscriptionDate, subscriptionDate) ||
                other.subscriptionDate == subscriptionDate));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, expirationDate, subscriptionDate);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ExpirationDateChangedImplCopyWith<_$ExpirationDateChangedImpl>
      get copyWith => __$$ExpirationDateChangedImplCopyWithImpl<
          _$ExpirationDateChangedImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String nameStr) nameChanged,
    required TResult Function(String costStr) costChanged,
    required TResult Function(String siteUrlStr) siteUrlChanged,
    required TResult Function(String noteStr) noteChanged,
    required TResult Function(
            DateTime subscriptionDate, DateTime expirationDate)
        subscriptionDateChanged,
    required TResult Function(
            DateTime expirationDate, DateTime subscriptionDate)
        expirationDateChanged,
    required TResult Function(String? iconPath) iconPathChanged,
    required TResult Function() sendMeNotificationsPressed,
  }) {
    return expirationDateChanged(expirationDate, subscriptionDate);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String nameStr)? nameChanged,
    TResult? Function(String costStr)? costChanged,
    TResult? Function(String siteUrlStr)? siteUrlChanged,
    TResult? Function(String noteStr)? noteChanged,
    TResult? Function(DateTime subscriptionDate, DateTime expirationDate)?
        subscriptionDateChanged,
    TResult? Function(DateTime expirationDate, DateTime subscriptionDate)?
        expirationDateChanged,
    TResult? Function(String? iconPath)? iconPathChanged,
    TResult? Function()? sendMeNotificationsPressed,
  }) {
    return expirationDateChanged?.call(expirationDate, subscriptionDate);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String nameStr)? nameChanged,
    TResult Function(String costStr)? costChanged,
    TResult Function(String siteUrlStr)? siteUrlChanged,
    TResult Function(String noteStr)? noteChanged,
    TResult Function(DateTime subscriptionDate, DateTime expirationDate)?
        subscriptionDateChanged,
    TResult Function(DateTime expirationDate, DateTime subscriptionDate)?
        expirationDateChanged,
    TResult Function(String? iconPath)? iconPathChanged,
    TResult Function()? sendMeNotificationsPressed,
    required TResult orElse(),
  }) {
    if (expirationDateChanged != null) {
      return expirationDateChanged(expirationDate, subscriptionDate);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NameChanged value) nameChanged,
    required TResult Function(_CostChanged value) costChanged,
    required TResult Function(_SiteUrlChanged value) siteUrlChanged,
    required TResult Function(_NoteChanged value) noteChanged,
    required TResult Function(_SubscriptionDateChanged value)
        subscriptionDateChanged,
    required TResult Function(_ExpirationDateChanged value)
        expirationDateChanged,
    required TResult Function(_IconPathChanged value) iconPathChanged,
    required TResult Function(_SendMeNotificationsPressed value)
        sendMeNotificationsPressed,
  }) {
    return expirationDateChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NameChanged value)? nameChanged,
    TResult? Function(_CostChanged value)? costChanged,
    TResult? Function(_SiteUrlChanged value)? siteUrlChanged,
    TResult? Function(_NoteChanged value)? noteChanged,
    TResult? Function(_SubscriptionDateChanged value)? subscriptionDateChanged,
    TResult? Function(_ExpirationDateChanged value)? expirationDateChanged,
    TResult? Function(_IconPathChanged value)? iconPathChanged,
    TResult? Function(_SendMeNotificationsPressed value)?
        sendMeNotificationsPressed,
  }) {
    return expirationDateChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NameChanged value)? nameChanged,
    TResult Function(_CostChanged value)? costChanged,
    TResult Function(_SiteUrlChanged value)? siteUrlChanged,
    TResult Function(_NoteChanged value)? noteChanged,
    TResult Function(_SubscriptionDateChanged value)? subscriptionDateChanged,
    TResult Function(_ExpirationDateChanged value)? expirationDateChanged,
    TResult Function(_IconPathChanged value)? iconPathChanged,
    TResult Function(_SendMeNotificationsPressed value)?
        sendMeNotificationsPressed,
    required TResult orElse(),
  }) {
    if (expirationDateChanged != null) {
      return expirationDateChanged(this);
    }
    return orElse();
  }
}

abstract class _ExpirationDateChanged implements AddSubscriptionFormEvent {
  const factory _ExpirationDateChanged(
      {required final DateTime expirationDate,
      required final DateTime subscriptionDate}) = _$ExpirationDateChangedImpl;

  DateTime get expirationDate;
  DateTime get subscriptionDate;
  @JsonKey(ignore: true)
  _$$ExpirationDateChangedImplCopyWith<_$ExpirationDateChangedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$IconPathChangedImplCopyWith<$Res> {
  factory _$$IconPathChangedImplCopyWith(_$IconPathChangedImpl value,
          $Res Function(_$IconPathChangedImpl) then) =
      __$$IconPathChangedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String? iconPath});
}

/// @nodoc
class __$$IconPathChangedImplCopyWithImpl<$Res>
    extends _$AddSubscriptionFormEventCopyWithImpl<$Res, _$IconPathChangedImpl>
    implements _$$IconPathChangedImplCopyWith<$Res> {
  __$$IconPathChangedImplCopyWithImpl(
      _$IconPathChangedImpl _value, $Res Function(_$IconPathChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? iconPath = freezed,
  }) {
    return _then(_$IconPathChangedImpl(
      freezed == iconPath
          ? _value.iconPath
          : iconPath // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$IconPathChangedImpl implements _IconPathChanged {
  const _$IconPathChangedImpl(this.iconPath);

  @override
  final String? iconPath;

  @override
  String toString() {
    return 'AddSubscriptionFormEvent.iconPathChanged(iconPath: $iconPath)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$IconPathChangedImpl &&
            (identical(other.iconPath, iconPath) ||
                other.iconPath == iconPath));
  }

  @override
  int get hashCode => Object.hash(runtimeType, iconPath);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$IconPathChangedImplCopyWith<_$IconPathChangedImpl> get copyWith =>
      __$$IconPathChangedImplCopyWithImpl<_$IconPathChangedImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String nameStr) nameChanged,
    required TResult Function(String costStr) costChanged,
    required TResult Function(String siteUrlStr) siteUrlChanged,
    required TResult Function(String noteStr) noteChanged,
    required TResult Function(
            DateTime subscriptionDate, DateTime expirationDate)
        subscriptionDateChanged,
    required TResult Function(
            DateTime expirationDate, DateTime subscriptionDate)
        expirationDateChanged,
    required TResult Function(String? iconPath) iconPathChanged,
    required TResult Function() sendMeNotificationsPressed,
  }) {
    return iconPathChanged(iconPath);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String nameStr)? nameChanged,
    TResult? Function(String costStr)? costChanged,
    TResult? Function(String siteUrlStr)? siteUrlChanged,
    TResult? Function(String noteStr)? noteChanged,
    TResult? Function(DateTime subscriptionDate, DateTime expirationDate)?
        subscriptionDateChanged,
    TResult? Function(DateTime expirationDate, DateTime subscriptionDate)?
        expirationDateChanged,
    TResult? Function(String? iconPath)? iconPathChanged,
    TResult? Function()? sendMeNotificationsPressed,
  }) {
    return iconPathChanged?.call(iconPath);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String nameStr)? nameChanged,
    TResult Function(String costStr)? costChanged,
    TResult Function(String siteUrlStr)? siteUrlChanged,
    TResult Function(String noteStr)? noteChanged,
    TResult Function(DateTime subscriptionDate, DateTime expirationDate)?
        subscriptionDateChanged,
    TResult Function(DateTime expirationDate, DateTime subscriptionDate)?
        expirationDateChanged,
    TResult Function(String? iconPath)? iconPathChanged,
    TResult Function()? sendMeNotificationsPressed,
    required TResult orElse(),
  }) {
    if (iconPathChanged != null) {
      return iconPathChanged(iconPath);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NameChanged value) nameChanged,
    required TResult Function(_CostChanged value) costChanged,
    required TResult Function(_SiteUrlChanged value) siteUrlChanged,
    required TResult Function(_NoteChanged value) noteChanged,
    required TResult Function(_SubscriptionDateChanged value)
        subscriptionDateChanged,
    required TResult Function(_ExpirationDateChanged value)
        expirationDateChanged,
    required TResult Function(_IconPathChanged value) iconPathChanged,
    required TResult Function(_SendMeNotificationsPressed value)
        sendMeNotificationsPressed,
  }) {
    return iconPathChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NameChanged value)? nameChanged,
    TResult? Function(_CostChanged value)? costChanged,
    TResult? Function(_SiteUrlChanged value)? siteUrlChanged,
    TResult? Function(_NoteChanged value)? noteChanged,
    TResult? Function(_SubscriptionDateChanged value)? subscriptionDateChanged,
    TResult? Function(_ExpirationDateChanged value)? expirationDateChanged,
    TResult? Function(_IconPathChanged value)? iconPathChanged,
    TResult? Function(_SendMeNotificationsPressed value)?
        sendMeNotificationsPressed,
  }) {
    return iconPathChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NameChanged value)? nameChanged,
    TResult Function(_CostChanged value)? costChanged,
    TResult Function(_SiteUrlChanged value)? siteUrlChanged,
    TResult Function(_NoteChanged value)? noteChanged,
    TResult Function(_SubscriptionDateChanged value)? subscriptionDateChanged,
    TResult Function(_ExpirationDateChanged value)? expirationDateChanged,
    TResult Function(_IconPathChanged value)? iconPathChanged,
    TResult Function(_SendMeNotificationsPressed value)?
        sendMeNotificationsPressed,
    required TResult orElse(),
  }) {
    if (iconPathChanged != null) {
      return iconPathChanged(this);
    }
    return orElse();
  }
}

abstract class _IconPathChanged implements AddSubscriptionFormEvent {
  const factory _IconPathChanged(final String? iconPath) =
      _$IconPathChangedImpl;

  String? get iconPath;
  @JsonKey(ignore: true)
  _$$IconPathChangedImplCopyWith<_$IconPathChangedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SendMeNotificationsPressedImplCopyWith<$Res> {
  factory _$$SendMeNotificationsPressedImplCopyWith(
          _$SendMeNotificationsPressedImpl value,
          $Res Function(_$SendMeNotificationsPressedImpl) then) =
      __$$SendMeNotificationsPressedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$SendMeNotificationsPressedImplCopyWithImpl<$Res>
    extends _$AddSubscriptionFormEventCopyWithImpl<$Res,
        _$SendMeNotificationsPressedImpl>
    implements _$$SendMeNotificationsPressedImplCopyWith<$Res> {
  __$$SendMeNotificationsPressedImplCopyWithImpl(
      _$SendMeNotificationsPressedImpl _value,
      $Res Function(_$SendMeNotificationsPressedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$SendMeNotificationsPressedImpl implements _SendMeNotificationsPressed {
  const _$SendMeNotificationsPressedImpl();

  @override
  String toString() {
    return 'AddSubscriptionFormEvent.sendMeNotificationsPressed()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SendMeNotificationsPressedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String nameStr) nameChanged,
    required TResult Function(String costStr) costChanged,
    required TResult Function(String siteUrlStr) siteUrlChanged,
    required TResult Function(String noteStr) noteChanged,
    required TResult Function(
            DateTime subscriptionDate, DateTime expirationDate)
        subscriptionDateChanged,
    required TResult Function(
            DateTime expirationDate, DateTime subscriptionDate)
        expirationDateChanged,
    required TResult Function(String? iconPath) iconPathChanged,
    required TResult Function() sendMeNotificationsPressed,
  }) {
    return sendMeNotificationsPressed();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String nameStr)? nameChanged,
    TResult? Function(String costStr)? costChanged,
    TResult? Function(String siteUrlStr)? siteUrlChanged,
    TResult? Function(String noteStr)? noteChanged,
    TResult? Function(DateTime subscriptionDate, DateTime expirationDate)?
        subscriptionDateChanged,
    TResult? Function(DateTime expirationDate, DateTime subscriptionDate)?
        expirationDateChanged,
    TResult? Function(String? iconPath)? iconPathChanged,
    TResult? Function()? sendMeNotificationsPressed,
  }) {
    return sendMeNotificationsPressed?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String nameStr)? nameChanged,
    TResult Function(String costStr)? costChanged,
    TResult Function(String siteUrlStr)? siteUrlChanged,
    TResult Function(String noteStr)? noteChanged,
    TResult Function(DateTime subscriptionDate, DateTime expirationDate)?
        subscriptionDateChanged,
    TResult Function(DateTime expirationDate, DateTime subscriptionDate)?
        expirationDateChanged,
    TResult Function(String? iconPath)? iconPathChanged,
    TResult Function()? sendMeNotificationsPressed,
    required TResult orElse(),
  }) {
    if (sendMeNotificationsPressed != null) {
      return sendMeNotificationsPressed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NameChanged value) nameChanged,
    required TResult Function(_CostChanged value) costChanged,
    required TResult Function(_SiteUrlChanged value) siteUrlChanged,
    required TResult Function(_NoteChanged value) noteChanged,
    required TResult Function(_SubscriptionDateChanged value)
        subscriptionDateChanged,
    required TResult Function(_ExpirationDateChanged value)
        expirationDateChanged,
    required TResult Function(_IconPathChanged value) iconPathChanged,
    required TResult Function(_SendMeNotificationsPressed value)
        sendMeNotificationsPressed,
  }) {
    return sendMeNotificationsPressed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NameChanged value)? nameChanged,
    TResult? Function(_CostChanged value)? costChanged,
    TResult? Function(_SiteUrlChanged value)? siteUrlChanged,
    TResult? Function(_NoteChanged value)? noteChanged,
    TResult? Function(_SubscriptionDateChanged value)? subscriptionDateChanged,
    TResult? Function(_ExpirationDateChanged value)? expirationDateChanged,
    TResult? Function(_IconPathChanged value)? iconPathChanged,
    TResult? Function(_SendMeNotificationsPressed value)?
        sendMeNotificationsPressed,
  }) {
    return sendMeNotificationsPressed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NameChanged value)? nameChanged,
    TResult Function(_CostChanged value)? costChanged,
    TResult Function(_SiteUrlChanged value)? siteUrlChanged,
    TResult Function(_NoteChanged value)? noteChanged,
    TResult Function(_SubscriptionDateChanged value)? subscriptionDateChanged,
    TResult Function(_ExpirationDateChanged value)? expirationDateChanged,
    TResult Function(_IconPathChanged value)? iconPathChanged,
    TResult Function(_SendMeNotificationsPressed value)?
        sendMeNotificationsPressed,
    required TResult orElse(),
  }) {
    if (sendMeNotificationsPressed != null) {
      return sendMeNotificationsPressed(this);
    }
    return orElse();
  }
}

abstract class _SendMeNotificationsPressed implements AddSubscriptionFormEvent {
  const factory _SendMeNotificationsPressed() =
      _$SendMeNotificationsPressedImpl;
}

/// @nodoc
mixin _$AddSubscriptionFormState {
  Name get name => throw _privateConstructorUsedError;
  Cost get cost => throw _privateConstructorUsedError;
  SiteUrl get siteUrl => throw _privateConstructorUsedError;
  String get note => throw _privateConstructorUsedError;
  SubscriptionDate get subscriptionDate => throw _privateConstructorUsedError;
  ExpirationDate get expirationDate => throw _privateConstructorUsedError;
  AutovalidateMode get showErrorMessages => throw _privateConstructorUsedError;
  Subscription? get validatedSubscription => throw _privateConstructorUsedError;
  String? get iconPath => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AddSubscriptionFormStateCopyWith<AddSubscriptionFormState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AddSubscriptionFormStateCopyWith<$Res> {
  factory $AddSubscriptionFormStateCopyWith(AddSubscriptionFormState value,
          $Res Function(AddSubscriptionFormState) then) =
      _$AddSubscriptionFormStateCopyWithImpl<$Res, AddSubscriptionFormState>;
  @useResult
  $Res call(
      {Name name,
      Cost cost,
      SiteUrl siteUrl,
      String note,
      SubscriptionDate subscriptionDate,
      ExpirationDate expirationDate,
      AutovalidateMode showErrorMessages,
      Subscription? validatedSubscription,
      String? iconPath});

  $SubscriptionCopyWith<$Res>? get validatedSubscription;
}

/// @nodoc
class _$AddSubscriptionFormStateCopyWithImpl<$Res,
        $Val extends AddSubscriptionFormState>
    implements $AddSubscriptionFormStateCopyWith<$Res> {
  _$AddSubscriptionFormStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? cost = null,
    Object? siteUrl = null,
    Object? note = null,
    Object? subscriptionDate = null,
    Object? expirationDate = null,
    Object? showErrorMessages = null,
    Object? validatedSubscription = freezed,
    Object? iconPath = freezed,
  }) {
    return _then(_value.copyWith(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as Name,
      cost: null == cost
          ? _value.cost
          : cost // ignore: cast_nullable_to_non_nullable
              as Cost,
      siteUrl: null == siteUrl
          ? _value.siteUrl
          : siteUrl // ignore: cast_nullable_to_non_nullable
              as SiteUrl,
      note: null == note
          ? _value.note
          : note // ignore: cast_nullable_to_non_nullable
              as String,
      subscriptionDate: null == subscriptionDate
          ? _value.subscriptionDate
          : subscriptionDate // ignore: cast_nullable_to_non_nullable
              as SubscriptionDate,
      expirationDate: null == expirationDate
          ? _value.expirationDate
          : expirationDate // ignore: cast_nullable_to_non_nullable
              as ExpirationDate,
      showErrorMessages: null == showErrorMessages
          ? _value.showErrorMessages
          : showErrorMessages // ignore: cast_nullable_to_non_nullable
              as AutovalidateMode,
      validatedSubscription: freezed == validatedSubscription
          ? _value.validatedSubscription
          : validatedSubscription // ignore: cast_nullable_to_non_nullable
              as Subscription?,
      iconPath: freezed == iconPath
          ? _value.iconPath
          : iconPath // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $SubscriptionCopyWith<$Res>? get validatedSubscription {
    if (_value.validatedSubscription == null) {
      return null;
    }

    return $SubscriptionCopyWith<$Res>(_value.validatedSubscription!, (value) {
      return _then(_value.copyWith(validatedSubscription: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$AddSubscriptionFormStateImplCopyWith<$Res>
    implements $AddSubscriptionFormStateCopyWith<$Res> {
  factory _$$AddSubscriptionFormStateImplCopyWith(
          _$AddSubscriptionFormStateImpl value,
          $Res Function(_$AddSubscriptionFormStateImpl) then) =
      __$$AddSubscriptionFormStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {Name name,
      Cost cost,
      SiteUrl siteUrl,
      String note,
      SubscriptionDate subscriptionDate,
      ExpirationDate expirationDate,
      AutovalidateMode showErrorMessages,
      Subscription? validatedSubscription,
      String? iconPath});

  @override
  $SubscriptionCopyWith<$Res>? get validatedSubscription;
}

/// @nodoc
class __$$AddSubscriptionFormStateImplCopyWithImpl<$Res>
    extends _$AddSubscriptionFormStateCopyWithImpl<$Res,
        _$AddSubscriptionFormStateImpl>
    implements _$$AddSubscriptionFormStateImplCopyWith<$Res> {
  __$$AddSubscriptionFormStateImplCopyWithImpl(
      _$AddSubscriptionFormStateImpl _value,
      $Res Function(_$AddSubscriptionFormStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? cost = null,
    Object? siteUrl = null,
    Object? note = null,
    Object? subscriptionDate = null,
    Object? expirationDate = null,
    Object? showErrorMessages = null,
    Object? validatedSubscription = freezed,
    Object? iconPath = freezed,
  }) {
    return _then(_$AddSubscriptionFormStateImpl(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as Name,
      cost: null == cost
          ? _value.cost
          : cost // ignore: cast_nullable_to_non_nullable
              as Cost,
      siteUrl: null == siteUrl
          ? _value.siteUrl
          : siteUrl // ignore: cast_nullable_to_non_nullable
              as SiteUrl,
      note: null == note
          ? _value.note
          : note // ignore: cast_nullable_to_non_nullable
              as String,
      subscriptionDate: null == subscriptionDate
          ? _value.subscriptionDate
          : subscriptionDate // ignore: cast_nullable_to_non_nullable
              as SubscriptionDate,
      expirationDate: null == expirationDate
          ? _value.expirationDate
          : expirationDate // ignore: cast_nullable_to_non_nullable
              as ExpirationDate,
      showErrorMessages: null == showErrorMessages
          ? _value.showErrorMessages
          : showErrorMessages // ignore: cast_nullable_to_non_nullable
              as AutovalidateMode,
      validatedSubscription: freezed == validatedSubscription
          ? _value.validatedSubscription
          : validatedSubscription // ignore: cast_nullable_to_non_nullable
              as Subscription?,
      iconPath: freezed == iconPath
          ? _value.iconPath
          : iconPath // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$AddSubscriptionFormStateImpl implements _AddSubscriptionFormState {
  const _$AddSubscriptionFormStateImpl(
      {required this.name,
      required this.cost,
      required this.siteUrl,
      required this.note,
      required this.subscriptionDate,
      required this.expirationDate,
      required this.showErrorMessages,
      required this.validatedSubscription,
      this.iconPath});

  @override
  final Name name;
  @override
  final Cost cost;
  @override
  final SiteUrl siteUrl;
  @override
  final String note;
  @override
  final SubscriptionDate subscriptionDate;
  @override
  final ExpirationDate expirationDate;
  @override
  final AutovalidateMode showErrorMessages;
  @override
  final Subscription? validatedSubscription;
  @override
  final String? iconPath;

  @override
  String toString() {
    return 'AddSubscriptionFormState(name: $name, cost: $cost, siteUrl: $siteUrl, note: $note, subscriptionDate: $subscriptionDate, expirationDate: $expirationDate, showErrorMessages: $showErrorMessages, validatedSubscription: $validatedSubscription, iconPath: $iconPath)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AddSubscriptionFormStateImpl &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.cost, cost) || other.cost == cost) &&
            (identical(other.siteUrl, siteUrl) || other.siteUrl == siteUrl) &&
            (identical(other.note, note) || other.note == note) &&
            (identical(other.subscriptionDate, subscriptionDate) ||
                other.subscriptionDate == subscriptionDate) &&
            (identical(other.expirationDate, expirationDate) ||
                other.expirationDate == expirationDate) &&
            (identical(other.showErrorMessages, showErrorMessages) ||
                other.showErrorMessages == showErrorMessages) &&
            (identical(other.validatedSubscription, validatedSubscription) ||
                other.validatedSubscription == validatedSubscription) &&
            (identical(other.iconPath, iconPath) ||
                other.iconPath == iconPath));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      name,
      cost,
      siteUrl,
      note,
      subscriptionDate,
      expirationDate,
      showErrorMessages,
      validatedSubscription,
      iconPath);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AddSubscriptionFormStateImplCopyWith<_$AddSubscriptionFormStateImpl>
      get copyWith => __$$AddSubscriptionFormStateImplCopyWithImpl<
          _$AddSubscriptionFormStateImpl>(this, _$identity);
}

abstract class _AddSubscriptionFormState implements AddSubscriptionFormState {
  const factory _AddSubscriptionFormState(
      {required final Name name,
      required final Cost cost,
      required final SiteUrl siteUrl,
      required final String note,
      required final SubscriptionDate subscriptionDate,
      required final ExpirationDate expirationDate,
      required final AutovalidateMode showErrorMessages,
      required final Subscription? validatedSubscription,
      final String? iconPath}) = _$AddSubscriptionFormStateImpl;

  @override
  Name get name;
  @override
  Cost get cost;
  @override
  SiteUrl get siteUrl;
  @override
  String get note;
  @override
  SubscriptionDate get subscriptionDate;
  @override
  ExpirationDate get expirationDate;
  @override
  AutovalidateMode get showErrorMessages;
  @override
  Subscription? get validatedSubscription;
  @override
  String? get iconPath;
  @override
  @JsonKey(ignore: true)
  _$$AddSubscriptionFormStateImplCopyWith<_$AddSubscriptionFormStateImpl>
      get copyWith => throw _privateConstructorUsedError;
}
