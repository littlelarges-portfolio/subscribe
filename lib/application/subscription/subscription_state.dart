part of 'subscription_bloc.dart';

@freezed
class SubscriptionState with _$SubscriptionState {
  const factory SubscriptionState({
    required KtList<Subscription> subscriptions,
  }) = _SubscriptionState;

  factory SubscriptionState.initial() =>
      const SubscriptionState(subscriptions: KtList.empty());
}
