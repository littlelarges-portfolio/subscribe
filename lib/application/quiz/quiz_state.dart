part of 'quiz_bloc.dart';

@freezed
class QuizState with _$QuizState {
  const factory QuizState({
    required KtList<Quiz> quizzes,
    required KtList<String> selectedAnswers,
  }) = _QuizState;

  factory QuizState.initial() => QuizState(
        quizzes: KtList.from([
          Quiz(
            question: 'What types of subscription services are you '
                'interested in monitoring with our app?',
            answers: KtList.from([
              'Movies',
              'Music',
              'Video games',
              'Others',
            ]),
          ),
          Quiz(
            question: 'How often do you typically subscribe to new services?',
            answers: KtList.from([
              'Monthly',
              'Quarterly',
              'Yearly',
              'Occasionally',
            ]),
          ),
          Quiz(
            question: 'What features are you looking for in a subscription '
                'monitoring app?',
            answers: KtList.from([
              'Automated renewal reminders',
              'Expense tracking',
              'Customizable alerts',
              'Integration with calendar apps',
            ]),
          ),
          Quiz(
            question: 'How many subscription services do you currently use?',
            answers: KtList.from([
              '1-3',
              '4-6',
              '7-9',
              '10+',
            ]),
          ),
          Quiz(
            question: 'Which platforms do you primarily use for '
                'subscribing to services?',
            answers: KtList.from([
              'iOS',
              'Android',
              'Web-based',
              'Others',
            ]),
          ),
          Quiz(
            question: 'What challenges do you face in managing '
                'your current subscriptions?',
            answers: KtList.from([
              'Forgetting renewal dates',
              'Difficulty tracking expenses',
              'Subscription overload',
              'Others',
            ]),
          ),
          Quiz(
            question: 'How important is it for you to have access '
                'to historical subscription data?',
            answers: KtList.from([
              'Very important',
              'Somewhat important',
              'Not very important',
              'Not important at all',
            ]),
          ),
          Quiz(
            question: 'Would you prefer a free version of the app with limited '
                'features or a paid version with premium features?',
            answers: KtList.from([
              'Free version',
              'Paid version',
              'Either, depending on features',
              "I'm not sure",
            ]),
          ),
          Quiz(
            question: 'Do you prefer receiving notifications via email, '
                'push notifications, or both?',
            answers: KtList.from([
              'Email',
              'Push notifications',
              'Both',
              'Neither',
            ]),
          ),
          Quiz(
            question:
                'Are you interested in community features such as sharing '
                'subscription recommendations or tips?',
            answers: KtList.from([
              'Yes, definitely',
              "Maybe, if they're useful",
              'No, not interested',
              "I'm not sure yet",
            ]),
          ),
        ]),
        selectedAnswers: const KtList.empty(),
      );
}
