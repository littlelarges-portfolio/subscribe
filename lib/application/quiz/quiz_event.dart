part of 'quiz_bloc.dart';

@freezed
class QuizEvent with _$QuizEvent {
  const factory QuizEvent.answerSelected({
    required String question,
    required String answer,
  }) = _AnswerSelected;
}
