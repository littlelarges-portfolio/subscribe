import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:subscribe_app/domain/quiz/quiz.dart';

part 'quiz_event.dart';
part 'quiz_state.dart';
part 'quiz_bloc.freezed.dart';

@injectable
class QuizBloc extends Bloc<QuizEvent, QuizState> {
  QuizBloc() : super(QuizState.initial()) {
    on<QuizEvent>((event, emit) {
      event.map(
        answerSelected: (e) {
          emit(
            state.copyWith(
              quizzes: state.quizzes.map((quiz) {
                if (quiz.question == e.question) {
                  return quiz.copyWith(selectedAnswer: e.answer);
                } else {
                  return quiz;
                }
              }),
            ),
          );
        },
      );
    });
  }
}
