part of 'settings_bloc.dart';

@freezed
class SettingsState with _$SettingsState {
  const factory SettingsState({
    required bool enabled,
  }) = _SettingsState;

  factory SettingsState.initial() => const SettingsState(enabled: true);
}
