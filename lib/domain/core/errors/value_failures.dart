// ignore_for_file: lines_longer_than_80_chars

import 'package:freezed_annotation/freezed_annotation.dart';

part 'value_failures.freezed.dart';

@freezed
sealed class ValueFailure<T> with _$ValueFailure<T> {
  const factory ValueFailure.subscription(SubscriptionValueFailure<T> f) =
      _Subscription<T>;
}

@freezed
sealed class SubscriptionValueFailure<T> with _$SubscriptionValueFailure<T> {
  const factory SubscriptionValueFailure.invalidCost({required T failedValue}) =
      _InvalidCost<T>;
  const factory SubscriptionValueFailure.cantBeEmpty({required T failedValue}) =
      _CantBeEmpty<T>;
  const factory SubscriptionValueFailure.subscriptionDateCantBeAfterExpirationDate({
    required T failedValue,
  }) = _SubscriptionDateCantBeAfterExpirationDate<T>;
  const factory SubscriptionValueFailure.expirationDateCantBeBeforeSubscriptionDate({
    required T failedValue,
  }) = _ExpirationDateCantBeBeforeSubscriptionDate<T>;
}
