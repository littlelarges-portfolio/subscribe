// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'value_failures.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$ValueFailure<T> {
  SubscriptionValueFailure<T> get f => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(SubscriptionValueFailure<T> f) subscription,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(SubscriptionValueFailure<T> f)? subscription,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(SubscriptionValueFailure<T> f)? subscription,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Subscription<T> value) subscription,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Subscription<T> value)? subscription,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Subscription<T> value)? subscription,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ValueFailureCopyWith<T, ValueFailure<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ValueFailureCopyWith<T, $Res> {
  factory $ValueFailureCopyWith(
          ValueFailure<T> value, $Res Function(ValueFailure<T>) then) =
      _$ValueFailureCopyWithImpl<T, $Res, ValueFailure<T>>;
  @useResult
  $Res call({SubscriptionValueFailure<T> f});

  $SubscriptionValueFailureCopyWith<T, $Res> get f;
}

/// @nodoc
class _$ValueFailureCopyWithImpl<T, $Res, $Val extends ValueFailure<T>>
    implements $ValueFailureCopyWith<T, $Res> {
  _$ValueFailureCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? f = null,
  }) {
    return _then(_value.copyWith(
      f: null == f
          ? _value.f
          : f // ignore: cast_nullable_to_non_nullable
              as SubscriptionValueFailure<T>,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $SubscriptionValueFailureCopyWith<T, $Res> get f {
    return $SubscriptionValueFailureCopyWith<T, $Res>(_value.f, (value) {
      return _then(_value.copyWith(f: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$SubscriptionImplCopyWith<T, $Res>
    implements $ValueFailureCopyWith<T, $Res> {
  factory _$$SubscriptionImplCopyWith(_$SubscriptionImpl<T> value,
          $Res Function(_$SubscriptionImpl<T>) then) =
      __$$SubscriptionImplCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({SubscriptionValueFailure<T> f});

  @override
  $SubscriptionValueFailureCopyWith<T, $Res> get f;
}

/// @nodoc
class __$$SubscriptionImplCopyWithImpl<T, $Res>
    extends _$ValueFailureCopyWithImpl<T, $Res, _$SubscriptionImpl<T>>
    implements _$$SubscriptionImplCopyWith<T, $Res> {
  __$$SubscriptionImplCopyWithImpl(
      _$SubscriptionImpl<T> _value, $Res Function(_$SubscriptionImpl<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? f = null,
  }) {
    return _then(_$SubscriptionImpl<T>(
      null == f
          ? _value.f
          : f // ignore: cast_nullable_to_non_nullable
              as SubscriptionValueFailure<T>,
    ));
  }
}

/// @nodoc

class _$SubscriptionImpl<T> implements _Subscription<T> {
  const _$SubscriptionImpl(this.f);

  @override
  final SubscriptionValueFailure<T> f;

  @override
  String toString() {
    return 'ValueFailure<$T>.subscription(f: $f)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SubscriptionImpl<T> &&
            (identical(other.f, f) || other.f == f));
  }

  @override
  int get hashCode => Object.hash(runtimeType, f);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SubscriptionImplCopyWith<T, _$SubscriptionImpl<T>> get copyWith =>
      __$$SubscriptionImplCopyWithImpl<T, _$SubscriptionImpl<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(SubscriptionValueFailure<T> f) subscription,
  }) {
    return subscription(f);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(SubscriptionValueFailure<T> f)? subscription,
  }) {
    return subscription?.call(f);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(SubscriptionValueFailure<T> f)? subscription,
    required TResult orElse(),
  }) {
    if (subscription != null) {
      return subscription(f);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Subscription<T> value) subscription,
  }) {
    return subscription(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Subscription<T> value)? subscription,
  }) {
    return subscription?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Subscription<T> value)? subscription,
    required TResult orElse(),
  }) {
    if (subscription != null) {
      return subscription(this);
    }
    return orElse();
  }
}

abstract class _Subscription<T> implements ValueFailure<T> {
  const factory _Subscription(final SubscriptionValueFailure<T> f) =
      _$SubscriptionImpl<T>;

  @override
  SubscriptionValueFailure<T> get f;
  @override
  @JsonKey(ignore: true)
  _$$SubscriptionImplCopyWith<T, _$SubscriptionImpl<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$SubscriptionValueFailure<T> {
  T get failedValue => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) invalidCost,
    required TResult Function(T failedValue) cantBeEmpty,
    required TResult Function(T failedValue)
        subscriptionDateCantBeAfterExpirationDate,
    required TResult Function(T failedValue)
        expirationDateCantBeBeforeSubscriptionDate,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T failedValue)? invalidCost,
    TResult? Function(T failedValue)? cantBeEmpty,
    TResult? Function(T failedValue)? subscriptionDateCantBeAfterExpirationDate,
    TResult? Function(T failedValue)?
        expirationDateCantBeBeforeSubscriptionDate,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? invalidCost,
    TResult Function(T failedValue)? cantBeEmpty,
    TResult Function(T failedValue)? subscriptionDateCantBeAfterExpirationDate,
    TResult Function(T failedValue)? expirationDateCantBeBeforeSubscriptionDate,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InvalidCost<T> value) invalidCost,
    required TResult Function(_CantBeEmpty<T> value) cantBeEmpty,
    required TResult Function(
            _SubscriptionDateCantBeAfterExpirationDate<T> value)
        subscriptionDateCantBeAfterExpirationDate,
    required TResult Function(
            _ExpirationDateCantBeBeforeSubscriptionDate<T> value)
        expirationDateCantBeBeforeSubscriptionDate,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InvalidCost<T> value)? invalidCost,
    TResult? Function(_CantBeEmpty<T> value)? cantBeEmpty,
    TResult? Function(_SubscriptionDateCantBeAfterExpirationDate<T> value)?
        subscriptionDateCantBeAfterExpirationDate,
    TResult? Function(_ExpirationDateCantBeBeforeSubscriptionDate<T> value)?
        expirationDateCantBeBeforeSubscriptionDate,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InvalidCost<T> value)? invalidCost,
    TResult Function(_CantBeEmpty<T> value)? cantBeEmpty,
    TResult Function(_SubscriptionDateCantBeAfterExpirationDate<T> value)?
        subscriptionDateCantBeAfterExpirationDate,
    TResult Function(_ExpirationDateCantBeBeforeSubscriptionDate<T> value)?
        expirationDateCantBeBeforeSubscriptionDate,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SubscriptionValueFailureCopyWith<T, SubscriptionValueFailure<T>>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SubscriptionValueFailureCopyWith<T, $Res> {
  factory $SubscriptionValueFailureCopyWith(SubscriptionValueFailure<T> value,
          $Res Function(SubscriptionValueFailure<T>) then) =
      _$SubscriptionValueFailureCopyWithImpl<T, $Res,
          SubscriptionValueFailure<T>>;
  @useResult
  $Res call({T failedValue});
}

/// @nodoc
class _$SubscriptionValueFailureCopyWithImpl<T, $Res,
        $Val extends SubscriptionValueFailure<T>>
    implements $SubscriptionValueFailureCopyWith<T, $Res> {
  _$SubscriptionValueFailureCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(_value.copyWith(
      failedValue: freezed == failedValue
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$InvalidCostImplCopyWith<T, $Res>
    implements $SubscriptionValueFailureCopyWith<T, $Res> {
  factory _$$InvalidCostImplCopyWith(_$InvalidCostImpl<T> value,
          $Res Function(_$InvalidCostImpl<T>) then) =
      __$$InvalidCostImplCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T failedValue});
}

/// @nodoc
class __$$InvalidCostImplCopyWithImpl<T, $Res>
    extends _$SubscriptionValueFailureCopyWithImpl<T, $Res,
        _$InvalidCostImpl<T>> implements _$$InvalidCostImplCopyWith<T, $Res> {
  __$$InvalidCostImplCopyWithImpl(
      _$InvalidCostImpl<T> _value, $Res Function(_$InvalidCostImpl<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(_$InvalidCostImpl<T>(
      failedValue: freezed == failedValue
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$InvalidCostImpl<T> implements _InvalidCost<T> {
  const _$InvalidCostImpl({required this.failedValue});

  @override
  final T failedValue;

  @override
  String toString() {
    return 'SubscriptionValueFailure<$T>.invalidCost(failedValue: $failedValue)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InvalidCostImpl<T> &&
            const DeepCollectionEquality()
                .equals(other.failedValue, failedValue));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failedValue));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$InvalidCostImplCopyWith<T, _$InvalidCostImpl<T>> get copyWith =>
      __$$InvalidCostImplCopyWithImpl<T, _$InvalidCostImpl<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) invalidCost,
    required TResult Function(T failedValue) cantBeEmpty,
    required TResult Function(T failedValue)
        subscriptionDateCantBeAfterExpirationDate,
    required TResult Function(T failedValue)
        expirationDateCantBeBeforeSubscriptionDate,
  }) {
    return invalidCost(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T failedValue)? invalidCost,
    TResult? Function(T failedValue)? cantBeEmpty,
    TResult? Function(T failedValue)? subscriptionDateCantBeAfterExpirationDate,
    TResult? Function(T failedValue)?
        expirationDateCantBeBeforeSubscriptionDate,
  }) {
    return invalidCost?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? invalidCost,
    TResult Function(T failedValue)? cantBeEmpty,
    TResult Function(T failedValue)? subscriptionDateCantBeAfterExpirationDate,
    TResult Function(T failedValue)? expirationDateCantBeBeforeSubscriptionDate,
    required TResult orElse(),
  }) {
    if (invalidCost != null) {
      return invalidCost(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InvalidCost<T> value) invalidCost,
    required TResult Function(_CantBeEmpty<T> value) cantBeEmpty,
    required TResult Function(
            _SubscriptionDateCantBeAfterExpirationDate<T> value)
        subscriptionDateCantBeAfterExpirationDate,
    required TResult Function(
            _ExpirationDateCantBeBeforeSubscriptionDate<T> value)
        expirationDateCantBeBeforeSubscriptionDate,
  }) {
    return invalidCost(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InvalidCost<T> value)? invalidCost,
    TResult? Function(_CantBeEmpty<T> value)? cantBeEmpty,
    TResult? Function(_SubscriptionDateCantBeAfterExpirationDate<T> value)?
        subscriptionDateCantBeAfterExpirationDate,
    TResult? Function(_ExpirationDateCantBeBeforeSubscriptionDate<T> value)?
        expirationDateCantBeBeforeSubscriptionDate,
  }) {
    return invalidCost?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InvalidCost<T> value)? invalidCost,
    TResult Function(_CantBeEmpty<T> value)? cantBeEmpty,
    TResult Function(_SubscriptionDateCantBeAfterExpirationDate<T> value)?
        subscriptionDateCantBeAfterExpirationDate,
    TResult Function(_ExpirationDateCantBeBeforeSubscriptionDate<T> value)?
        expirationDateCantBeBeforeSubscriptionDate,
    required TResult orElse(),
  }) {
    if (invalidCost != null) {
      return invalidCost(this);
    }
    return orElse();
  }
}

abstract class _InvalidCost<T> implements SubscriptionValueFailure<T> {
  const factory _InvalidCost({required final T failedValue}) =
      _$InvalidCostImpl<T>;

  @override
  T get failedValue;
  @override
  @JsonKey(ignore: true)
  _$$InvalidCostImplCopyWith<T, _$InvalidCostImpl<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$CantBeEmptyImplCopyWith<T, $Res>
    implements $SubscriptionValueFailureCopyWith<T, $Res> {
  factory _$$CantBeEmptyImplCopyWith(_$CantBeEmptyImpl<T> value,
          $Res Function(_$CantBeEmptyImpl<T>) then) =
      __$$CantBeEmptyImplCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T failedValue});
}

/// @nodoc
class __$$CantBeEmptyImplCopyWithImpl<T, $Res>
    extends _$SubscriptionValueFailureCopyWithImpl<T, $Res,
        _$CantBeEmptyImpl<T>> implements _$$CantBeEmptyImplCopyWith<T, $Res> {
  __$$CantBeEmptyImplCopyWithImpl(
      _$CantBeEmptyImpl<T> _value, $Res Function(_$CantBeEmptyImpl<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(_$CantBeEmptyImpl<T>(
      failedValue: freezed == failedValue
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$CantBeEmptyImpl<T> implements _CantBeEmpty<T> {
  const _$CantBeEmptyImpl({required this.failedValue});

  @override
  final T failedValue;

  @override
  String toString() {
    return 'SubscriptionValueFailure<$T>.cantBeEmpty(failedValue: $failedValue)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CantBeEmptyImpl<T> &&
            const DeepCollectionEquality()
                .equals(other.failedValue, failedValue));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failedValue));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CantBeEmptyImplCopyWith<T, _$CantBeEmptyImpl<T>> get copyWith =>
      __$$CantBeEmptyImplCopyWithImpl<T, _$CantBeEmptyImpl<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) invalidCost,
    required TResult Function(T failedValue) cantBeEmpty,
    required TResult Function(T failedValue)
        subscriptionDateCantBeAfterExpirationDate,
    required TResult Function(T failedValue)
        expirationDateCantBeBeforeSubscriptionDate,
  }) {
    return cantBeEmpty(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T failedValue)? invalidCost,
    TResult? Function(T failedValue)? cantBeEmpty,
    TResult? Function(T failedValue)? subscriptionDateCantBeAfterExpirationDate,
    TResult? Function(T failedValue)?
        expirationDateCantBeBeforeSubscriptionDate,
  }) {
    return cantBeEmpty?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? invalidCost,
    TResult Function(T failedValue)? cantBeEmpty,
    TResult Function(T failedValue)? subscriptionDateCantBeAfterExpirationDate,
    TResult Function(T failedValue)? expirationDateCantBeBeforeSubscriptionDate,
    required TResult orElse(),
  }) {
    if (cantBeEmpty != null) {
      return cantBeEmpty(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InvalidCost<T> value) invalidCost,
    required TResult Function(_CantBeEmpty<T> value) cantBeEmpty,
    required TResult Function(
            _SubscriptionDateCantBeAfterExpirationDate<T> value)
        subscriptionDateCantBeAfterExpirationDate,
    required TResult Function(
            _ExpirationDateCantBeBeforeSubscriptionDate<T> value)
        expirationDateCantBeBeforeSubscriptionDate,
  }) {
    return cantBeEmpty(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InvalidCost<T> value)? invalidCost,
    TResult? Function(_CantBeEmpty<T> value)? cantBeEmpty,
    TResult? Function(_SubscriptionDateCantBeAfterExpirationDate<T> value)?
        subscriptionDateCantBeAfterExpirationDate,
    TResult? Function(_ExpirationDateCantBeBeforeSubscriptionDate<T> value)?
        expirationDateCantBeBeforeSubscriptionDate,
  }) {
    return cantBeEmpty?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InvalidCost<T> value)? invalidCost,
    TResult Function(_CantBeEmpty<T> value)? cantBeEmpty,
    TResult Function(_SubscriptionDateCantBeAfterExpirationDate<T> value)?
        subscriptionDateCantBeAfterExpirationDate,
    TResult Function(_ExpirationDateCantBeBeforeSubscriptionDate<T> value)?
        expirationDateCantBeBeforeSubscriptionDate,
    required TResult orElse(),
  }) {
    if (cantBeEmpty != null) {
      return cantBeEmpty(this);
    }
    return orElse();
  }
}

abstract class _CantBeEmpty<T> implements SubscriptionValueFailure<T> {
  const factory _CantBeEmpty({required final T failedValue}) =
      _$CantBeEmptyImpl<T>;

  @override
  T get failedValue;
  @override
  @JsonKey(ignore: true)
  _$$CantBeEmptyImplCopyWith<T, _$CantBeEmptyImpl<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SubscriptionDateCantBeAfterExpirationDateImplCopyWith<T, $Res>
    implements $SubscriptionValueFailureCopyWith<T, $Res> {
  factory _$$SubscriptionDateCantBeAfterExpirationDateImplCopyWith(
          _$SubscriptionDateCantBeAfterExpirationDateImpl<T> value,
          $Res Function(_$SubscriptionDateCantBeAfterExpirationDateImpl<T>)
              then) =
      __$$SubscriptionDateCantBeAfterExpirationDateImplCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T failedValue});
}

/// @nodoc
class __$$SubscriptionDateCantBeAfterExpirationDateImplCopyWithImpl<T, $Res>
    extends _$SubscriptionValueFailureCopyWithImpl<T, $Res,
        _$SubscriptionDateCantBeAfterExpirationDateImpl<T>>
    implements
        _$$SubscriptionDateCantBeAfterExpirationDateImplCopyWith<T, $Res> {
  __$$SubscriptionDateCantBeAfterExpirationDateImplCopyWithImpl(
      _$SubscriptionDateCantBeAfterExpirationDateImpl<T> _value,
      $Res Function(_$SubscriptionDateCantBeAfterExpirationDateImpl<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(_$SubscriptionDateCantBeAfterExpirationDateImpl<T>(
      failedValue: freezed == failedValue
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$SubscriptionDateCantBeAfterExpirationDateImpl<T>
    implements _SubscriptionDateCantBeAfterExpirationDate<T> {
  const _$SubscriptionDateCantBeAfterExpirationDateImpl(
      {required this.failedValue});

  @override
  final T failedValue;

  @override
  String toString() {
    return 'SubscriptionValueFailure<$T>.subscriptionDateCantBeAfterExpirationDate(failedValue: $failedValue)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SubscriptionDateCantBeAfterExpirationDateImpl<T> &&
            const DeepCollectionEquality()
                .equals(other.failedValue, failedValue));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failedValue));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SubscriptionDateCantBeAfterExpirationDateImplCopyWith<T,
          _$SubscriptionDateCantBeAfterExpirationDateImpl<T>>
      get copyWith =>
          __$$SubscriptionDateCantBeAfterExpirationDateImplCopyWithImpl<T,
                  _$SubscriptionDateCantBeAfterExpirationDateImpl<T>>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) invalidCost,
    required TResult Function(T failedValue) cantBeEmpty,
    required TResult Function(T failedValue)
        subscriptionDateCantBeAfterExpirationDate,
    required TResult Function(T failedValue)
        expirationDateCantBeBeforeSubscriptionDate,
  }) {
    return subscriptionDateCantBeAfterExpirationDate(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T failedValue)? invalidCost,
    TResult? Function(T failedValue)? cantBeEmpty,
    TResult? Function(T failedValue)? subscriptionDateCantBeAfterExpirationDate,
    TResult? Function(T failedValue)?
        expirationDateCantBeBeforeSubscriptionDate,
  }) {
    return subscriptionDateCantBeAfterExpirationDate?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? invalidCost,
    TResult Function(T failedValue)? cantBeEmpty,
    TResult Function(T failedValue)? subscriptionDateCantBeAfterExpirationDate,
    TResult Function(T failedValue)? expirationDateCantBeBeforeSubscriptionDate,
    required TResult orElse(),
  }) {
    if (subscriptionDateCantBeAfterExpirationDate != null) {
      return subscriptionDateCantBeAfterExpirationDate(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InvalidCost<T> value) invalidCost,
    required TResult Function(_CantBeEmpty<T> value) cantBeEmpty,
    required TResult Function(
            _SubscriptionDateCantBeAfterExpirationDate<T> value)
        subscriptionDateCantBeAfterExpirationDate,
    required TResult Function(
            _ExpirationDateCantBeBeforeSubscriptionDate<T> value)
        expirationDateCantBeBeforeSubscriptionDate,
  }) {
    return subscriptionDateCantBeAfterExpirationDate(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InvalidCost<T> value)? invalidCost,
    TResult? Function(_CantBeEmpty<T> value)? cantBeEmpty,
    TResult? Function(_SubscriptionDateCantBeAfterExpirationDate<T> value)?
        subscriptionDateCantBeAfterExpirationDate,
    TResult? Function(_ExpirationDateCantBeBeforeSubscriptionDate<T> value)?
        expirationDateCantBeBeforeSubscriptionDate,
  }) {
    return subscriptionDateCantBeAfterExpirationDate?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InvalidCost<T> value)? invalidCost,
    TResult Function(_CantBeEmpty<T> value)? cantBeEmpty,
    TResult Function(_SubscriptionDateCantBeAfterExpirationDate<T> value)?
        subscriptionDateCantBeAfterExpirationDate,
    TResult Function(_ExpirationDateCantBeBeforeSubscriptionDate<T> value)?
        expirationDateCantBeBeforeSubscriptionDate,
    required TResult orElse(),
  }) {
    if (subscriptionDateCantBeAfterExpirationDate != null) {
      return subscriptionDateCantBeAfterExpirationDate(this);
    }
    return orElse();
  }
}

abstract class _SubscriptionDateCantBeAfterExpirationDate<T>
    implements SubscriptionValueFailure<T> {
  const factory _SubscriptionDateCantBeAfterExpirationDate(
          {required final T failedValue}) =
      _$SubscriptionDateCantBeAfterExpirationDateImpl<T>;

  @override
  T get failedValue;
  @override
  @JsonKey(ignore: true)
  _$$SubscriptionDateCantBeAfterExpirationDateImplCopyWith<T,
          _$SubscriptionDateCantBeAfterExpirationDateImpl<T>>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ExpirationDateCantBeBeforeSubscriptionDateImplCopyWith<T,
    $Res> implements $SubscriptionValueFailureCopyWith<T, $Res> {
  factory _$$ExpirationDateCantBeBeforeSubscriptionDateImplCopyWith(
          _$ExpirationDateCantBeBeforeSubscriptionDateImpl<T> value,
          $Res Function(_$ExpirationDateCantBeBeforeSubscriptionDateImpl<T>)
              then) =
      __$$ExpirationDateCantBeBeforeSubscriptionDateImplCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T failedValue});
}

/// @nodoc
class __$$ExpirationDateCantBeBeforeSubscriptionDateImplCopyWithImpl<T, $Res>
    extends _$SubscriptionValueFailureCopyWithImpl<T, $Res,
        _$ExpirationDateCantBeBeforeSubscriptionDateImpl<T>>
    implements
        _$$ExpirationDateCantBeBeforeSubscriptionDateImplCopyWith<T, $Res> {
  __$$ExpirationDateCantBeBeforeSubscriptionDateImplCopyWithImpl(
      _$ExpirationDateCantBeBeforeSubscriptionDateImpl<T> _value,
      $Res Function(_$ExpirationDateCantBeBeforeSubscriptionDateImpl<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(_$ExpirationDateCantBeBeforeSubscriptionDateImpl<T>(
      failedValue: freezed == failedValue
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$ExpirationDateCantBeBeforeSubscriptionDateImpl<T>
    implements _ExpirationDateCantBeBeforeSubscriptionDate<T> {
  const _$ExpirationDateCantBeBeforeSubscriptionDateImpl(
      {required this.failedValue});

  @override
  final T failedValue;

  @override
  String toString() {
    return 'SubscriptionValueFailure<$T>.expirationDateCantBeBeforeSubscriptionDate(failedValue: $failedValue)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ExpirationDateCantBeBeforeSubscriptionDateImpl<T> &&
            const DeepCollectionEquality()
                .equals(other.failedValue, failedValue));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failedValue));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ExpirationDateCantBeBeforeSubscriptionDateImplCopyWith<T,
          _$ExpirationDateCantBeBeforeSubscriptionDateImpl<T>>
      get copyWith =>
          __$$ExpirationDateCantBeBeforeSubscriptionDateImplCopyWithImpl<T,
                  _$ExpirationDateCantBeBeforeSubscriptionDateImpl<T>>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) invalidCost,
    required TResult Function(T failedValue) cantBeEmpty,
    required TResult Function(T failedValue)
        subscriptionDateCantBeAfterExpirationDate,
    required TResult Function(T failedValue)
        expirationDateCantBeBeforeSubscriptionDate,
  }) {
    return expirationDateCantBeBeforeSubscriptionDate(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T failedValue)? invalidCost,
    TResult? Function(T failedValue)? cantBeEmpty,
    TResult? Function(T failedValue)? subscriptionDateCantBeAfterExpirationDate,
    TResult? Function(T failedValue)?
        expirationDateCantBeBeforeSubscriptionDate,
  }) {
    return expirationDateCantBeBeforeSubscriptionDate?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? invalidCost,
    TResult Function(T failedValue)? cantBeEmpty,
    TResult Function(T failedValue)? subscriptionDateCantBeAfterExpirationDate,
    TResult Function(T failedValue)? expirationDateCantBeBeforeSubscriptionDate,
    required TResult orElse(),
  }) {
    if (expirationDateCantBeBeforeSubscriptionDate != null) {
      return expirationDateCantBeBeforeSubscriptionDate(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InvalidCost<T> value) invalidCost,
    required TResult Function(_CantBeEmpty<T> value) cantBeEmpty,
    required TResult Function(
            _SubscriptionDateCantBeAfterExpirationDate<T> value)
        subscriptionDateCantBeAfterExpirationDate,
    required TResult Function(
            _ExpirationDateCantBeBeforeSubscriptionDate<T> value)
        expirationDateCantBeBeforeSubscriptionDate,
  }) {
    return expirationDateCantBeBeforeSubscriptionDate(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InvalidCost<T> value)? invalidCost,
    TResult? Function(_CantBeEmpty<T> value)? cantBeEmpty,
    TResult? Function(_SubscriptionDateCantBeAfterExpirationDate<T> value)?
        subscriptionDateCantBeAfterExpirationDate,
    TResult? Function(_ExpirationDateCantBeBeforeSubscriptionDate<T> value)?
        expirationDateCantBeBeforeSubscriptionDate,
  }) {
    return expirationDateCantBeBeforeSubscriptionDate?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InvalidCost<T> value)? invalidCost,
    TResult Function(_CantBeEmpty<T> value)? cantBeEmpty,
    TResult Function(_SubscriptionDateCantBeAfterExpirationDate<T> value)?
        subscriptionDateCantBeAfterExpirationDate,
    TResult Function(_ExpirationDateCantBeBeforeSubscriptionDate<T> value)?
        expirationDateCantBeBeforeSubscriptionDate,
    required TResult orElse(),
  }) {
    if (expirationDateCantBeBeforeSubscriptionDate != null) {
      return expirationDateCantBeBeforeSubscriptionDate(this);
    }
    return orElse();
  }
}

abstract class _ExpirationDateCantBeBeforeSubscriptionDate<T>
    implements SubscriptionValueFailure<T> {
  const factory _ExpirationDateCantBeBeforeSubscriptionDate(
          {required final T failedValue}) =
      _$ExpirationDateCantBeBeforeSubscriptionDateImpl<T>;

  @override
  T get failedValue;
  @override
  @JsonKey(ignore: true)
  _$$ExpirationDateCantBeBeforeSubscriptionDateImplCopyWith<T,
          _$ExpirationDateCantBeBeforeSubscriptionDateImpl<T>>
      get copyWith => throw _privateConstructorUsedError;
}
