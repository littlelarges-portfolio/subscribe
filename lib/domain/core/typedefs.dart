import 'package:dartz/dartz.dart';
import 'package:subscribe_app/domain/core/errors/value_failures.dart';

typedef Value<T> = Either<ValueFailure<T>, T>;
