import 'package:dartz/dartz.dart';
import 'package:subscribe_app/domain/core/errors/value_failures.dart';
import 'package:subscribe_app/domain/core/typedefs.dart';

Value<String> validateCost(
  String input,
) {
  final regExp = RegExp(r'^[1-9]\d*$');

  if (regExp.hasMatch(input)) {
    return right(input);
  } else {
    return left(
      ValueFailure.subscription(
        SubscriptionValueFailure.invalidCost(
          failedValue: input,
        ),
      ),
    );
  }
}

Value<String> validateEmpty(
  String input,
) {
  if (input.isNotEmpty) {
    return right(input);
  } else {
    return left(
      ValueFailure.subscription(
        SubscriptionValueFailure.cantBeEmpty(
          failedValue: input,
        ),
      ),
    );
  }
}

Value<DateTime> validateDateEmpty(
  DateTime input,
) {
  if (input != DateTime(0)) {
    return right(input);
  } else {
    return left(
      ValueFailure.subscription(
        SubscriptionValueFailure.cantBeEmpty(
          failedValue: input,
        ),
      ),
    );
  }
}

Value<DateTime> validateSubscriptionDateCantBeAfterExpirationDate(
  DateTime subscriptionDate,
  DateTime expirationDate,
) {
  if (!subscriptionDate.isAfter(expirationDate) &&
      expirationDate != DateTime(0) &&
      subscriptionDate != DateTime(0)) {
    return right(subscriptionDate);
  } else if (subscriptionDate == DateTime(0)) {
    return left(
      ValueFailure.subscription(
        SubscriptionValueFailure.cantBeEmpty(
          failedValue: subscriptionDate,
        ),
      ),
    );
  } else {
    return left(
      ValueFailure.subscription(
        SubscriptionValueFailure.subscriptionDateCantBeAfterExpirationDate(
          failedValue: subscriptionDate,
        ),
      ),
    );
  }
}

Value<DateTime> validateExpirationDateCantBeBeforeSubscriptionDate(
  DateTime expirationDate,
  DateTime subscriptionDate,
) {
  if (!expirationDate.isBefore(subscriptionDate) &&
      subscriptionDate != DateTime(0) &&
      expirationDate != DateTime(0)) {
    return right(expirationDate);
  } else if (expirationDate == DateTime(0)) {
    return left(
      ValueFailure.subscription(
        SubscriptionValueFailure.cantBeEmpty(
          failedValue: expirationDate,
        ),
      ),
    );
  } else {
    return left(
      ValueFailure.subscription(
        SubscriptionValueFailure.expirationDateCantBeBeforeSubscriptionDate(
          failedValue: expirationDate,
        ),
      ),
    );
  }
}
