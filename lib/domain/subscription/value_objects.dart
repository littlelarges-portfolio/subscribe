import 'package:subscribe_app/domain/core/value_objects.dart';
import 'package:subscribe_app/domain/core/value_validators.dart';

class Name extends ValueObject<String> {
  factory Name(String input) => Name._(validateEmpty(input));

  const Name._(super.value);
}

class Cost extends ValueObject<String> {
  factory Cost(String input) => Cost._(validateCost(input));

  const Cost._(super.value);
}

class SiteUrl extends ValueObject<String> {
  factory SiteUrl(String input) => SiteUrl._(validateEmpty(input));

  const SiteUrl._(super.value);
}

class SubscriptionDate extends ValueObject<DateTime> {
  factory SubscriptionDate({
    required DateTime subscriptionDate,
    required DateTime expirationDate,
  }) =>
      SubscriptionDate._(
        validateDateEmpty(subscriptionDate).flatMap(
          (f) => validateSubscriptionDateCantBeAfterExpirationDate(
            subscriptionDate,
            expirationDate,
          ),
        ),
      );

  const SubscriptionDate._(super.value);
}

class ExpirationDate extends ValueObject<DateTime> {
  factory ExpirationDate({
    required DateTime expirationDate,
    required DateTime subscriptionDate,
  }) =>
      ExpirationDate._(
        validateDateEmpty(expirationDate).flatMap(
          (f) => validateExpirationDateCantBeBeforeSubscriptionDate(
            expirationDate,
            subscriptionDate,
          ),
        ),
      );

  const ExpirationDate._(super.value);
}
