import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:subscribe_app/domain/subscription/value_objects.dart';

part 'subscription.freezed.dart';

@freezed
abstract class Subscription with _$Subscription {
  const factory Subscription({
    required Name name,
    required Cost cost,
    required SiteUrl siteUrl,
    required String note,
    required SubscriptionDate subscriptionDate,
    required ExpirationDate expirationDate,
    String? iconPath,
  }) = _Subscription;
}
