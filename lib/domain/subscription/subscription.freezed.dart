// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'subscription.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$Subscription {
  Name get name => throw _privateConstructorUsedError;
  Cost get cost => throw _privateConstructorUsedError;
  SiteUrl get siteUrl => throw _privateConstructorUsedError;
  String get note => throw _privateConstructorUsedError;
  SubscriptionDate get subscriptionDate => throw _privateConstructorUsedError;
  ExpirationDate get expirationDate => throw _privateConstructorUsedError;
  String? get iconPath => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SubscriptionCopyWith<Subscription> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SubscriptionCopyWith<$Res> {
  factory $SubscriptionCopyWith(
          Subscription value, $Res Function(Subscription) then) =
      _$SubscriptionCopyWithImpl<$Res, Subscription>;
  @useResult
  $Res call(
      {Name name,
      Cost cost,
      SiteUrl siteUrl,
      String note,
      SubscriptionDate subscriptionDate,
      ExpirationDate expirationDate,
      String? iconPath});
}

/// @nodoc
class _$SubscriptionCopyWithImpl<$Res, $Val extends Subscription>
    implements $SubscriptionCopyWith<$Res> {
  _$SubscriptionCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? cost = null,
    Object? siteUrl = null,
    Object? note = null,
    Object? subscriptionDate = null,
    Object? expirationDate = null,
    Object? iconPath = freezed,
  }) {
    return _then(_value.copyWith(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as Name,
      cost: null == cost
          ? _value.cost
          : cost // ignore: cast_nullable_to_non_nullable
              as Cost,
      siteUrl: null == siteUrl
          ? _value.siteUrl
          : siteUrl // ignore: cast_nullable_to_non_nullable
              as SiteUrl,
      note: null == note
          ? _value.note
          : note // ignore: cast_nullable_to_non_nullable
              as String,
      subscriptionDate: null == subscriptionDate
          ? _value.subscriptionDate
          : subscriptionDate // ignore: cast_nullable_to_non_nullable
              as SubscriptionDate,
      expirationDate: null == expirationDate
          ? _value.expirationDate
          : expirationDate // ignore: cast_nullable_to_non_nullable
              as ExpirationDate,
      iconPath: freezed == iconPath
          ? _value.iconPath
          : iconPath // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SubscriptionImplCopyWith<$Res>
    implements $SubscriptionCopyWith<$Res> {
  factory _$$SubscriptionImplCopyWith(
          _$SubscriptionImpl value, $Res Function(_$SubscriptionImpl) then) =
      __$$SubscriptionImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {Name name,
      Cost cost,
      SiteUrl siteUrl,
      String note,
      SubscriptionDate subscriptionDate,
      ExpirationDate expirationDate,
      String? iconPath});
}

/// @nodoc
class __$$SubscriptionImplCopyWithImpl<$Res>
    extends _$SubscriptionCopyWithImpl<$Res, _$SubscriptionImpl>
    implements _$$SubscriptionImplCopyWith<$Res> {
  __$$SubscriptionImplCopyWithImpl(
      _$SubscriptionImpl _value, $Res Function(_$SubscriptionImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? cost = null,
    Object? siteUrl = null,
    Object? note = null,
    Object? subscriptionDate = null,
    Object? expirationDate = null,
    Object? iconPath = freezed,
  }) {
    return _then(_$SubscriptionImpl(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as Name,
      cost: null == cost
          ? _value.cost
          : cost // ignore: cast_nullable_to_non_nullable
              as Cost,
      siteUrl: null == siteUrl
          ? _value.siteUrl
          : siteUrl // ignore: cast_nullable_to_non_nullable
              as SiteUrl,
      note: null == note
          ? _value.note
          : note // ignore: cast_nullable_to_non_nullable
              as String,
      subscriptionDate: null == subscriptionDate
          ? _value.subscriptionDate
          : subscriptionDate // ignore: cast_nullable_to_non_nullable
              as SubscriptionDate,
      expirationDate: null == expirationDate
          ? _value.expirationDate
          : expirationDate // ignore: cast_nullable_to_non_nullable
              as ExpirationDate,
      iconPath: freezed == iconPath
          ? _value.iconPath
          : iconPath // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$SubscriptionImpl implements _Subscription {
  const _$SubscriptionImpl(
      {required this.name,
      required this.cost,
      required this.siteUrl,
      required this.note,
      required this.subscriptionDate,
      required this.expirationDate,
      this.iconPath});

  @override
  final Name name;
  @override
  final Cost cost;
  @override
  final SiteUrl siteUrl;
  @override
  final String note;
  @override
  final SubscriptionDate subscriptionDate;
  @override
  final ExpirationDate expirationDate;
  @override
  final String? iconPath;

  @override
  String toString() {
    return 'Subscription(name: $name, cost: $cost, siteUrl: $siteUrl, note: $note, subscriptionDate: $subscriptionDate, expirationDate: $expirationDate, iconPath: $iconPath)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SubscriptionImpl &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.cost, cost) || other.cost == cost) &&
            (identical(other.siteUrl, siteUrl) || other.siteUrl == siteUrl) &&
            (identical(other.note, note) || other.note == note) &&
            (identical(other.subscriptionDate, subscriptionDate) ||
                other.subscriptionDate == subscriptionDate) &&
            (identical(other.expirationDate, expirationDate) ||
                other.expirationDate == expirationDate) &&
            (identical(other.iconPath, iconPath) ||
                other.iconPath == iconPath));
  }

  @override
  int get hashCode => Object.hash(runtimeType, name, cost, siteUrl, note,
      subscriptionDate, expirationDate, iconPath);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SubscriptionImplCopyWith<_$SubscriptionImpl> get copyWith =>
      __$$SubscriptionImplCopyWithImpl<_$SubscriptionImpl>(this, _$identity);
}

abstract class _Subscription implements Subscription {
  const factory _Subscription(
      {required final Name name,
      required final Cost cost,
      required final SiteUrl siteUrl,
      required final String note,
      required final SubscriptionDate subscriptionDate,
      required final ExpirationDate expirationDate,
      final String? iconPath}) = _$SubscriptionImpl;

  @override
  Name get name;
  @override
  Cost get cost;
  @override
  SiteUrl get siteUrl;
  @override
  String get note;
  @override
  SubscriptionDate get subscriptionDate;
  @override
  ExpirationDate get expirationDate;
  @override
  String? get iconPath;
  @override
  @JsonKey(ignore: true)
  _$$SubscriptionImplCopyWith<_$SubscriptionImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
