import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:subscribe_app/presentation/core/colours.dart';
import 'package:subscribe_app/presentation/core/gen/fonts.gen.dart';
import 'package:subscribe_app/presentation/core/helpers.dart';

extension NumExtension on num {
  double get fr => r * .85;
}

abstract class TextStyles {
  //* SF Pro Text

  static TextStyle get sfProText11MediumGrey => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 11.fr,
        fontWeight: FontWeight.w400,
        color: Colours.grey6,
        letterSpacing: -.08.r,
      );

  static TextStyle get sfProText11MediumBlack => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 11.fr,
        fontWeight: FontWeight.w400,
        color: Colours.black,
        letterSpacing: -.08.r,
      );

  static TextStyle get sfProText14RegularWhite => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 14.fr,
        fontWeight: FontWeight.w400,
        color: Colours.white,
        letterSpacing: -.32.r,
      );

  static TextStyle get sfProText14RegularLightBlue => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 14.fr,
        fontWeight: FontWeight.w400,
        color: Colours.lightBlue,
        letterSpacing: -.32.r,
      );

  static TextStyle get sfProText14RegularLightBlueWithOpacity05 => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 14.fr,
        fontWeight: FontWeight.w400,
        color: Colours.lightBlueWithOpacity05,
        letterSpacing: -.32.r,
      );

  static TextStyle get sfProText14MediumWhite => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 14.fr,
        fontWeight: FontWeight.w500,
        color: Colours.white,
        letterSpacing: -.08.r,
      );
  static TextStyle get sfProText14MediumBlack => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 14.fr,
        fontWeight: FontWeight.w500,
        color: Colours.black,
        letterSpacing: -.08.r,
      );

  static TextStyle get sfProText14SemiboldWhite => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 14.fr,
        fontWeight: FontWeight.w600,
        color: Colours.white,
        letterSpacing: -.32.r,
      );

  static TextStyle get sfProText16RegularWhite => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 16.fr,
        fontWeight: FontWeight.normal,
        color: Colours.white,
        letterSpacing: -.32.r,
      );

  static TextStyle get sfProText16MediumWhite => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 16.fr,
        fontWeight: FontWeight.w500,
        color: Colours.white,
        letterSpacing: -.32.r,
      );

  static TextStyle get sfProText16MediumGrey => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 16.fr,
        fontWeight: FontWeight.w500,
        color: Colours.grey5,
        letterSpacing: -.32.r,
      );

  static TextStyle get sfProText16SemiboldBlue => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 16.fr,
        fontWeight: FontWeight.w600,
        color: Colours.blue2,
        letterSpacing: -.32.r,
      );

  static TextStyle get sfProText16SemiboldGrey => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 16.fr,
        fontWeight: FontWeight.w600,
        color: Colours.grey,
        letterSpacing: -.32.r,
      );

  static TextStyle get sfProText16SemiboldWhite => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 16.fr,
        fontWeight: FontWeight.w600,
        color: Colours.white,
        letterSpacing: -.32.r,
      );

  static TextStyle get sfProText20RegularWhite => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 20.fr,
        fontWeight: FontWeight.normal,
        color: Colours.white,
        letterSpacing: -.32.r,
      );

  static TextStyle get sfProText20MediumWhite => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 20.fr,
        fontWeight: FontWeight.w500,
        color: Colours.white,
        letterSpacing: -.32.r,
      );

  static TextStyle get sfProText20SemiboldWhite => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 20.fr,
        fontWeight: FontWeight.w600,
        color: Colours.white,
        letterSpacing: -.32.r,
      );

  static TextStyle get sfProText24SemiboldWhite => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 24.fr,
        fontWeight: FontWeight.w600,
        color: Colours.white,
        letterSpacing: -.32.r,
      );

  static TextStyle get sfProText32SemiboldWhite => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 32.fr,
        fontWeight: FontWeight.w600,
        color: Colours.white,
        letterSpacing: -.32.r,
      );

  static TextStyle get sfProText32SemiboldGrey => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 32.fr,
        fontWeight: FontWeight.w600,
        color: Colours.grey,
        letterSpacing: -.32.r,
      );

  static TextStyle get sfProText32SemiboldLS212LH139White => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 32.fr,
        fontWeight: FontWeight.w600,
        color: Colours.white,
        letterSpacing: -2.12.r,
        height: 139.r.toFigmaHeight(32.r),
      );

  //* SF Pro Display

  static TextStyle get sfProDisplay20SemiboldWhite => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 20.fr,
        fontWeight: FontWeight.w600,
        color: Colours.white,
        letterSpacing: -.32.r,
      );

  //* Inter

  static TextStyle get inter14RegularWhite => TextStyle(
        fontFamily: FontFamily.inter,
        fontSize: 14.fr,
        fontWeight: FontWeight.normal,
        color: Colours.white,
      );

  static TextStyle get inter17RegularBlack => TextStyle(
        fontFamily: FontFamily.inter,
        fontSize: 17.fr,
        fontWeight: FontWeight.normal,
        color: Colours.black,
      );
}
