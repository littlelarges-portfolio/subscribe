import 'package:flutter/material.dart';

abstract class Colours {
  static Color get background => const Color(0xFF232323);
  static Color get white => Colors.white;
  static Color get black => Colors.black;
  static Color get lightBlue => const Color(0xFF4CA1FE);
  static Color get blue => const Color(0xFF32338D);
  static Color get lightBlueWithOpacity03 =>
      const Color(0xFF4CA1FE).withOpacity(.3);
  static Color get lightBlueWithOpacity05 =>
      const Color(0xFF4CA1FE).withOpacity(.5);
  static Color get lightBlueWithOpacity08 =>
      const Color(0xFF4CA1FE).withOpacity(.8);
  static Color get blueWithOpacity03 => const Color(0xFF32338D).withOpacity(.3);
  static Color get lightBlue2 => const Color(0xFF4C9FFC);
  static Color get lightBlue2WithOpacity05 =>
      const Color(0xFF4C9FFC).withOpacity(.5);
  static Color get blue3WithOpacity05 =>
      const Color(0xFF4B9CF9).withOpacity(.5);
  static Color get blackWithOpacity025 =>
      const Color(0xFF000000).withOpacity(.25);
  static Color get greyWithOpacity05 => Colors.grey.withOpacity(.5);
  static Color get periwinkle => const Color(0xFFCBCDFF);
  static Color get blue2 => const Color(0xFF007AFF);
  static Color get grey => const Color(0xFFC4C4C4);
  static Color get grey2 => const Color(0xFF3B3B3B);
  static Color get grey3 => const Color(0xFFD7D7D7);
  static Color get grey4 => const Color(0xFF9E9E9E);
  static Color get blue3 => const Color(0xFF42B7F9);
  static Color get grey5 => const Color(0xFFD4D4D4);
  static Color get grey6 => const Color(0xFF4A4A4A);
  static Color get minks => const Color(0xFF33358F);
  static Color get grey7 => const Color(0xFF444444);
  static Color get grey8 => const Color(0xFF828282);
  static Color get grey9 => const Color(0xFF1B1B1B);
  static Color get red => const Color(0xFFFF4F4F);
}
