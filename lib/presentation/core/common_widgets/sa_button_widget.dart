import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:subscribe_app/presentation/core/colours.dart';
import 'package:subscribe_app/presentation/core/text_styles.dart';

class SAButton extends StatelessWidget {
  const SAButton({
    required this.text,
    this.onTap,
    this.rounded = true,
    super.key,
  });

  final String text;
  final void Function()? onTap;
  final bool rounded;

  static final LinearGradient _gradient = LinearGradient(
    colors: [
      Colours.lightBlue,
      Colours.blue,
    ],
  );
  static final LinearGradient _disabledGradient = LinearGradient(
    colors: [
      Colours.greyWithOpacity05,
      Colours.greyWithOpacity05,
    ],
  );
  BorderRadius get _borderRadius => rounded
      ? BorderRadius.all(Radius.circular(27.r))
      : BorderRadius.all(Radius.circular(12.r));

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: 25.r,
      ),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: onTap != null ? _gradient : _disabledGradient,
          borderRadius: _borderRadius,
        ),
        child: ElevatedButton(
          onPressed: onTap,
          style: ElevatedButton.styleFrom(
            backgroundColor: Colors.transparent,
            shadowColor: Colors.transparent,
            shape: RoundedRectangleBorder(borderRadius: _borderRadius),
          ),
          child: Text(text, style: TextStyles.sfProDisplay20SemiboldWhite),
        ),
      ),
    );
  }
}
