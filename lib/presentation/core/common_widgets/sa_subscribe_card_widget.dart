import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gap/gap.dart';
import 'package:subscribe_app/application/subscription/subscription_bloc.dart';
import 'package:subscribe_app/domain/subscription/subscription.dart';
import 'package:subscribe_app/presentation/core/colours.dart';
import 'package:subscribe_app/presentation/core/gen/assets.gen.dart';
import 'package:subscribe_app/presentation/core/helpers.dart';
import 'package:subscribe_app/presentation/core/text_styles.dart';

class SASubscribeCard extends StatelessWidget {
  const SASubscribeCard({
    required this.subscription,
    required this.type,
    super.key,
  });

  final Subscription subscription;
  final CardType type;

  String get activeCardText {
    final subscriptionDate = subscription.subscriptionDate.getOrCrash();

    return 'Last paid \$${subscription.cost.getOrCrash()} on '
        '${subscriptionDate.day} '
        '${subscriptionDate.month.toNamedMonthString()}, '
        '${subscriptionDate.year}';
  }

  String get calendarCardText {
    final expirationDate = subscription.expirationDate.getOrCrash();

    return '${subscription.cost.getOrCrash()}\$ on '
        '${expirationDate.day} ${expirationDate.month.toNamedMonthString()}, '
        '${expirationDate.year}';
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 71.r,
      decoration: BoxDecoration(
        color: Colours.grey2,
        borderRadius: BorderRadius.circular(12.r),
      ),
      child: Stack(
        children: [
          Positioned(
            right: 0,
            top: 0,
            child: PopupMenuButton(
              color: Colours.background,
              icon: Assets.icons.expand.svg(),
              itemBuilder: (context) {
                return [
                  PopupMenuItem<dynamic>(
                    onTap: () {
                      context
                          .read<SubscriptionBloc>()
                          .add(SubscriptionEvent.removed(subscription));
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Delete',
                          style: TextStyles.sfProText14MediumWhite,
                        ),
                        Icon(Icons.delete, color: Colours.white),
                      ],
                    ),
                  ),
                ];
              },
            ),
          ),
          Row(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 12.r,
                ),
                child: subscription.iconPath != null
                    ? SvgPicture.asset(
                        subscription.iconPath!,
                        height: 50.r,
                      )
                    : Container(),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    subscription.name.getOrCrash(),
                    style: TextStyles.sfProText20MediumWhite,
                  ),
                  Gap(7.r),
                  Text(
                    type == CardType.active ? activeCardText : calendarCardText,
                    style: TextStyles.sfProText14MediumWhite,
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}

enum CardType {
  active,
  calendar,
}
