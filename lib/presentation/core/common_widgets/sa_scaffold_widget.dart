import 'package:flutter/material.dart';
import 'package:subscribe_app/presentation/core/colours.dart';

class SAScaffold extends StatelessWidget {
  const SAScaffold({
    super.key,
    this.appBar,
    this.body,
  });

  final AppBar? appBar;
  final Widget? body;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colours.background,
      appBar: appBar,
      body: body != null ? SafeArea(child: body!) : Container(),
    );
  }
}
