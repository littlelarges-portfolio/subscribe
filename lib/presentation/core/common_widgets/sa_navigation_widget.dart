import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:subscribe_app/presentation/core/colours.dart';

class SANavigation extends StatelessWidget {
  const SANavigation({
    required this.itemCount,
    required this.selectedItem,
    required this.onTap,
    super.key,
    this.circleRadius,
  });

  final int itemCount;
  final int selectedItem;
  final ValueChanged<int> onTap;
  final double? circleRadius;

  double get _circleRadius => circleRadius != null ? circleRadius! : 8.r;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: _circleRadius,
      width: 1.sw,
      child: Center(
        child: ListView.separated(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: itemCount,
          itemBuilder: (context, index) => GestureDetector(
            onTap: () => onTap(index),
            child: NavigationCircle(
              circleRadius: _circleRadius,
              isSelected: index == selectedItem,
            ),
          ),
          separatorBuilder: (context, index) => Gap(8.r),
        ),
      ),
    );
  }
}

class NavigationCircle extends StatelessWidget {
  const NavigationCircle({
    this.circleRadius,
    super.key,
    this.isSelected = false,
  });

  final double? circleRadius;
  final bool isSelected;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: circleRadius ?? 8.r,
      width: circleRadius ?? 8.r,
      decoration: BoxDecoration(
        color:
            isSelected ? Colours.lightBlue2 : Colours.lightBlue2WithOpacity05,
        shape: BoxShape.circle,
      ),
    );
  }
}
