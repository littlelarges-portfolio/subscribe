import 'package:intl/intl.dart';

extension FigmaDimention on double {
  double toFigmaHeight(double fontSize) {
    return this / fontSize;
  }
}

extension DateTimeFormatter on DateTime {
  String toFormattedDate() {
    return DateFormat('dd-MM-yyyy')
        .format(this)
        .replaceAll('-', '.')
        .split(' ')[0];
  }
}

extension IntMonthToStringConverter on int {
  String toNamedMonthString() {
    switch (this) {
      case 1:
        return 'January';
      case 2:
        return 'February';
      case 3:
        return 'March';
      case 4:
        return 'April';
      case 5:
        return 'May';
      case 6:
        return 'June';
      case 7:
        return 'July';
      case 8:
        return 'August';
      case 9:
        return 'September';
      case 10:
        return 'October';
      case 11:
        return 'November';
      case 12:
        return 'December';
      default:
        return 'Wrong statement!';
    }
  }
}

extension DateTimeNormalizer on DateTime {
  DateTime normalize() => DateTime(year, month, day);
}
