import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gap/gap.dart';
import 'package:subscribe_app/application/subscription/subscription_bloc.dart';
import 'package:subscribe_app/presentation/core/common_widgets/sa_scaffold_widget.dart';
import 'package:subscribe_app/presentation/core/gen/assets.gen.dart';
import 'package:subscribe_app/presentation/core/helpers.dart';
import 'package:subscribe_app/presentation/core/text_styles.dart';
import 'package:subscribe_app/presentation/notification/widgets/notification_card_widget.dart';
import 'package:subscribe_app/presentation/routes/router.dart';

class NotificationPage extends StatelessWidget {
  const NotificationPage({super.key});

  static const route = '/notification';

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SubscriptionBloc, SubscriptionState>(
      builder: (context, state) {
        return SAScaffold(
          body: Padding(
            padding: EdgeInsets.only(
              right: 25.r,
              left: 25.r,
            ),
            child: SizedBox(
              width: 1.sw,
              child: Stack(
                children: [
                  Positioned(
                    right: 0,
                    child: Padding(
                      padding: EdgeInsets.only(top: 24.r),
                      child: IconButton(
                        onPressed: () {
                          AddSubscriptionRoute()
                              .push<AddSubscriptionRoute>(context);
                        },
                        icon: Assets.icons.addSubscription.svg(
                          height: 34.r,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 32.r),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Notifications',
                          style: TextStyles.sfProText32SemiboldWhite,
                        ),
                        Gap(44.r),
                        Expanded(
                          child: ListView.separated(
                            shrinkWrap: true,
                            itemBuilder: (context, index) => NotificationCard(
                              title:
                                  state.subscriptions[index].name.getOrCrash(),
                              subtitle: state
                                  .subscriptions[index].expirationDate
                                  .getOrCrash()
                                  .toFormattedDate(),
                              icon: state.subscriptions[index].iconPath != null
                                  ? SvgPicture.asset(
                                      state.subscriptions[index].iconPath!,
                                    )
                                  : Container(),
                            ),
                            separatorBuilder: (context, index) => Gap(32.r),
                            itemCount: state.subscriptions.size,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
