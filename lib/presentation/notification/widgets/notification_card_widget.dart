import 'package:flutter/cupertino.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:subscribe_app/presentation/core/colours.dart';
import 'package:subscribe_app/presentation/core/gen/assets.gen.dart';
import 'package:subscribe_app/presentation/core/text_styles.dart';

class NotificationCard extends HookWidget {
  const NotificationCard({
    required this.title,
    required this.subtitle,
    required this.icon,
    super.key,
  });

  final String title;
  final String subtitle;
  final Widget icon;

  @override
  Widget build(BuildContext context) {
    final notificationEnabled = useState(true);
    return Container(
      height: 71.r,
      decoration: BoxDecoration(
        color: Colours.grey2,
        borderRadius: BorderRadius.circular(12.r),
      ),
      child: Stack(
        children: [
          Positioned(
            right: 12.r,
            top: 0,
            bottom: 0,
            child: CupertinoSwitch(
              value: notificationEnabled.value,
              activeColor: Colours.lightBlueWithOpacity08,
              onChanged: (value) {
                notificationEnabled.value = value;
              },
            ),
          ),
          Row(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 12.r,
                ),
                child: icon,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: TextStyles.sfProText20MediumWhite,
                  ),
                  Gap(7.r),
                  Row(
                    children: [
                      Assets.icons.miniCalendar.svg(),
                      Gap(9.r),
                      Text(
                        subtitle,
                        style: TextStyles.sfProText14RegularLightBlue,
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
