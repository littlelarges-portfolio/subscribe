import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:subscribe_app/presentation/core/colours.dart';
import 'package:subscribe_app/presentation/core/gen/assets.gen.dart';
import 'package:subscribe_app/presentation/core/text_styles.dart';

class ShellPage extends StatelessWidget {
  const ShellPage({
    required this.navigationShell,
    required this.children,
    super.key,
  });

  final StatefulNavigationShell navigationShell;
  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
        ),
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          selectedItemColor: Colours.lightBlue,
          selectedLabelStyle: TextStyles.sfProText14RegularLightBlue,
          unselectedLabelStyle:
              TextStyles.sfProText14RegularLightBlueWithOpacity05,
          unselectedItemColor: Colours.lightBlueWithOpacity05,
          backgroundColor: Colours.grey9,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              activeIcon: Assets.icons.home.svg(
                colorFilter: ColorFilter.mode(
                  Colours.lightBlue,
                  BlendMode.srcIn,
                ),
              ),
              icon: Assets.icons.home.svg(
                colorFilter: ColorFilter.mode(
                  Colours.lightBlueWithOpacity05,
                  BlendMode.srcIn,
                ),
              ),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              activeIcon: Assets.icons.calendar.svg(
                colorFilter: ColorFilter.mode(
                  Colours.lightBlue,
                  BlendMode.srcIn,
                ),
              ),
              icon: Assets.icons.calendar.svg(
                colorFilter: ColorFilter.mode(
                  Colours.lightBlueWithOpacity05,
                  BlendMode.srcIn,
                ),
              ),
              label: 'Calendar',
            ),
            BottomNavigationBarItem(
              activeIcon: Assets.icons.notification.svg(
                colorFilter: ColorFilter.mode(
                  Colours.lightBlue,
                  BlendMode.srcIn,
                ),
              ),
              icon: Assets.icons.notification.svg(
                colorFilter: ColorFilter.mode(
                  Colours.lightBlueWithOpacity05,
                  BlendMode.srcIn,
                ),
              ),
              label: 'Notification',
            ),
            BottomNavigationBarItem(
              activeIcon: Assets.icons.settings.svg(
                colorFilter: ColorFilter.mode(
                  Colours.lightBlue,
                  BlendMode.srcIn,
                ),
              ),
              icon: Assets.icons.settings.svg(
                colorFilter: ColorFilter.mode(
                  Colours.lightBlueWithOpacity05,
                  BlendMode.srcIn,
                ),
              ),
              label: 'Settings',
            ),
          ],
          currentIndex: navigationShell.currentIndex,
          onTap: (int index) {
            navigationShell.goBranch(
              index,
              initialLocation: index == navigationShell.currentIndex,
            );
          },
        ),
      ),
      body: IndexedStack(
        index: navigationShell.currentIndex,
        children: children,
      ),
    );
  }
}
