// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:kt_dart/kt.dart';

import 'package:subscribe_app/presentation/core/colours.dart';
import 'package:subscribe_app/presentation/core/text_styles.dart';

class Quiz extends StatelessWidget {
  const Quiz({
    required this.question,
    required this.answers,
    required this.onSelect,
    required this.selectedAnswer,
    super.key,
  });

  final String question;
  final KtList<String> answers;
  final String selectedAnswer;
  final ValueChanged<int> onSelect;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 25.r),
      child: Column(
        children: [
          Question(question: question),
          Gap(55.r),
          Answers(
            answers: answers,
            onSelect: onSelect,
            selectedAnswer: selectedAnswer,
          ),
        ],
      ),
    );
  }
}

class Question extends StatelessWidget {
  const Question({
    required this.question,
    super.key,
  });

  final String question;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      // width: 340.r,
      child: Text(
        question,
        style: TextStyles.sfProText24SemiboldWhite,
        textAlign: TextAlign.start,
      ),
    );
  }
}

class Answers extends StatelessWidget {
  const Answers({
    required this.answers,
    required this.onSelect,
    required this.selectedAnswer,
    super.key,
  });

  final KtList<String> answers;
  final String selectedAnswer;
  final ValueChanged<int> onSelect;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) => GestureDetector(
        onTap: () => onSelect(index),
        child: Container(
          height: 41.r,
          width: 343.r,
          decoration: BoxDecoration(
            color: Colours.blue3WithOpacity05,
            borderRadius: BorderRadius.all(Radius.circular(12.r)),
            border: selectedAnswer == answers[index]
                ? Border.all(width: 2.r, color: Colours.periwinkle)
                : null,
            boxShadow: [
              BoxShadow(
                color: Colours.blackWithOpacity025,
                blurRadius: 4,
                offset: const Offset(0, 4),
              ),
            ],
          ),
          child: Center(
            child: Text(
              answers[index],
              style: TextStyles.sfProDisplay20SemiboldWhite,
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
      separatorBuilder: (context, index) => Gap(30.r),
      itemCount: 4,
    );
  }
}
