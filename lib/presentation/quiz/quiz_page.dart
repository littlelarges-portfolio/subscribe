// ignore_for_file: lines_longer_than_80_chars

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:subscribe_app/application/quiz/quiz_bloc.dart';
import 'package:subscribe_app/injection.dart';
import 'package:subscribe_app/presentation/core/common_widgets/sa_button_widget.dart';
import 'package:subscribe_app/presentation/core/common_widgets/sa_navigation_widget.dart';
import 'package:subscribe_app/presentation/core/common_widgets/sa_scaffold_widget.dart';
import 'package:subscribe_app/presentation/quiz/widgets/quiz_widget.dart';
import 'package:subscribe_app/presentation/routes/router.dart';

class QuizPage extends HookWidget {
  const QuizPage({super.key});

  static const route = '/quiz';

  @override
  Widget build(BuildContext context) {
    final pageController = usePageController();
    final selectedQuiz = useState(0);
    return BlocProvider(
      create: (context) => getIt<QuizBloc>(),
      child: SAScaffold(
        body: Padding(
          padding: EdgeInsets.only(
            bottom: 55.r,
            top: 20.r,
          ),
          child: BlocBuilder<QuizBloc, QuizState>(
            builder: (context, state) {
              return Column(
                children: [
                  SANavigation(
                    itemCount: 10,
                    selectedItem: selectedQuiz.value,
                    onTap: (value) {
                      selectedQuiz.value = value;
                    },
                  ),
                  Gap(40.r),
                  Expanded(
                    child: PageView(
                      onPageChanged: (value) => selectedQuiz.value = value,
                      controller: pageController,
                      physics: const NeverScrollableScrollPhysics(),
                      children: List.generate(
                        state.quizzes.size,
                        (index) => Quiz(
                          question: state.quizzes[selectedQuiz.value].question,
                          answers: state.quizzes[selectedQuiz.value].answers,
                          onSelect: (index) => context.read<QuizBloc>().add(
                                QuizEvent.answerSelected(
                                  question: state
                                      .quizzes[selectedQuiz.value].question,
                                  answer: state.quizzes[selectedQuiz.value]
                                      .answers[index],
                                ),
                              ),
                          selectedAnswer: state
                                  .quizzes[selectedQuiz.value].selectedAnswer ??
                              '',
                        ),
                      ),
                    ),
                  ),
                  SAButton(
                    text: 'Continue',
                    onTap: state.quizzes[selectedQuiz.value].selectedAnswer !=
                            null
                        ? () {
                            if (selectedQuiz.value != state.quizzes.size - 1) {
                              pageController.jumpToPage(selectedQuiz.value + 1);
                            } else {
                              PersonaficationRoute().go(context);
                            }
                          }
                        : null,
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
