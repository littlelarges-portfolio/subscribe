import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:subscribe_app/presentation/core/colours.dart';

class CustomCircleLoadingIndicator extends HookWidget {
  const CustomCircleLoadingIndicator({super.key});

  @override
  Widget build(BuildContext context) {
    final controller = useAnimationController(
      duration: const Duration(seconds: 3),
    )..repeat();

    final curvedAnimation =
        CurvedAnimation(parent: controller, curve: Curves.easeInOutCirc);

    return Stack(
      children: [
        CustomPaint(
          painter: _CircleBackgroundPainter(),
          child: SizedBox(
            width: 159.r,
            height: 159.r,
          ),
        ),
        AnimatedBuilder(
          animation: curvedAnimation,
          builder: (context, child) {
            return Transform.rotate(
              angle: curvedAnimation.value * 2.0 * pi,
              child: child,
            );
          },
          child: CustomPaint(
            painter: _CirclePainter(),
            child: SizedBox(
              width: 159.r,
              height: 159.r,
            ),
          ),
        ),
      ],
    );
  }
}

class _CirclePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final center = Offset(size.width / 2, size.height / 2);
    final radius = size.width / 2.25;

    const startAngle = -.75 * pi;
    const sweepAngle = .5 * pi;

    final paint = Paint()
      ..shader = LinearGradient(
        colors: [
          Colours.lightBlue,
          Colours.blue,
        ],
      ).createShader(Rect.fromCircle(center: center, radius: radius))
      ..strokeWidth = 25.r
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;

    canvas.drawArc(
      Rect.fromCircle(center: center, radius: radius),
      startAngle,
      sweepAngle,
      false,
      paint,
    );
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}

class _CircleBackgroundPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final center = Offset(size.width / 2, size.height / 2);
    final radius = size.width /  2.25;

    const startAngle = 1 * pi;
    const sweepAngle = 2 * pi;

    final paint = Paint()
      ..shader = LinearGradient(
        colors: [
          Colours.lightBlueWithOpacity03,
          Colours.blueWithOpacity03,
        ],
      ).createShader(Rect.fromCircle(center: center, radius: radius))
      ..strokeWidth = 25.r
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;

    canvas.drawArc(
      Rect.fromCircle(center: center, radius: radius),
      startAngle,
      sweepAngle,
      false,
      paint,
    );
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
