import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:subscribe_app/presentation/core/common_widgets/sa_button_widget.dart';
import 'package:subscribe_app/presentation/core/common_widgets/sa_scaffold_widget.dart';
import 'package:subscribe_app/presentation/core/text_styles.dart';
import 'package:subscribe_app/presentation/personafication/widgets/custom_circle_loading_indicator.dart';
import 'package:subscribe_app/presentation/routes/router.dart';

class PersonaficationPage extends HookWidget {
  const PersonaficationPage({super.key});

  static const route = '/personafication';

  static const _personaficationDetails = [
    'Curating your ideal interface',
    'Molding the app to your taste',
    'Shaping every detail to fit',
  ];

  static const _animationDuration = Duration(milliseconds: 200);

  @override
  Widget build(BuildContext context) {
    final shownItems = useState(0);
    final continueButtonIsHidden = useState(true);

    void showContinueButton() {
      continueButtonIsHidden.value = false;
    }

    useEffect(
      () {
        Future<void> delayedFunction(int index) async {
          if (index < _personaficationDetails.length) {
            await Future<void>.delayed(const Duration(seconds: 3));
            shownItems.value += 1;
            await delayedFunction(index + 1);
          } else if (index == _personaficationDetails.length) {
            await Future<void>.delayed(const Duration(seconds: 3));
            showContinueButton();
          }
        }

        delayedFunction(0);
        return () {};
      },
      [],
    );

    return SAScaffold(
      body: Padding(
        padding: EdgeInsets.only(
          top: 18.r,
          bottom: 55.r,
        ),
        child: SizedBox(
          width: 1.sw,
          child: Column(
            children: [
              Text(
                'Personafication',
                style: TextStyles.sfProText32SemiboldLS212LH139White,
                textAlign: TextAlign.center,
              ),
              const CustomCircleLoadingIndicator(),
              Gap(30.r),
              SizedBox(
                width: 255.r,
                child: Text(
                  'Please wait \n'
                  'Now we will configure the app based on your answers',
                  style: TextStyles.sfProText16RegularWhite,
                  textAlign: TextAlign.center,
                ),
              ),
              Gap(90.r),
              ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) => AnimatedOpacity(
                  opacity: index < shownItems.value ? 1 : 0,
                  duration: _animationDuration,
                  child: Center(
                    child: Text(
                      _personaficationDetails[index],
                      style: TextStyles.sfProText16SemiboldBlue,
                    ),
                  ),
                ),
                separatorBuilder: (context, index) => Gap(
                  10.r,
                ),
                itemCount: _personaficationDetails.length,
              ),
              const Spacer(),
              AnimatedOpacity(
                opacity: continueButtonIsHidden.value ? 0 : 1,
                duration: _animationDuration,
                child: SAButton(
                  text: 'Continue',
                  onTap: () {
                    const HomeRoute().go(context);
                  },
                  rounded: false,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
