import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:subscribe_app/presentation/core/text_styles.dart';

class TitleAndSubtitle extends StatelessWidget {
  const TitleAndSubtitle({
    required this.title,
    required this.subtitle,
    super.key,
  });

  final String title;
  final String subtitle;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 287.r,
      child: Column(
        children: [
          Text(
            title,
            style: TextStyles.sfProText32SemiboldWhite,
            textAlign: TextAlign.center,
          ),
          Gap(15.r),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 16.5.r),
            child: Text(
              subtitle,
              style: TextStyles.sfProText14RegularWhite,
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }
}
