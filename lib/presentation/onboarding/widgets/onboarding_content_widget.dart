import 'package:flutter/material.dart';
import 'package:subscribe_app/presentation/onboarding/widgets/onboarding_content_title_and_subtitle_widget.dart';

class OnBoardingContent extends StatelessWidget {
  const OnBoardingContent({
    required this.title,
    required this.subtitle,
    required this.image,
    super.key,
  });

  final String title;
  final String subtitle;
  final Image image;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        image,
        TitleAndSubtitle(title: title, subtitle: subtitle),
      ],
    );
  }
}
