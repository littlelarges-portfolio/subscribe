import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:subscribe_app/presentation/core/colours.dart';
import 'package:subscribe_app/presentation/core/common_widgets/sa_button_widget.dart';
import 'package:subscribe_app/presentation/core/common_widgets/sa_navigation_widget.dart';
import 'package:subscribe_app/presentation/core/common_widgets/sa_scaffold_widget.dart';
import 'package:subscribe_app/presentation/core/gen/assets.gen.dart';
import 'package:subscribe_app/presentation/core/text_styles.dart';
import 'package:subscribe_app/presentation/onboarding/widgets/onboarding_content_widget.dart';
import 'package:subscribe_app/presentation/routes/router.dart';

class OnBoardingPage extends HookWidget {
  const OnBoardingPage({super.key});

  static const route = '/onboarding';

  static const _pageSwitchingDuration = Duration(milliseconds: 200);

  @override
  Widget build(BuildContext context) {
    final pageController = usePageController();
    final currentPage = useState(0);

    return SAScaffold(
      appBar: AppBar(
        backgroundColor: Colours.background,
        actions: [
          TextButton(
            onPressed: () {
              QuizRoute().go(context);
            },
            child: Text(
              'Skip',
              style: TextStyles.sfProText14SemiboldWhite,
            ),
          ),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.only(
          bottom: 50.r,
        ),
        child: Column(
          children: [
            Expanded(
              child: PageView(
                controller: pageController,
                onPageChanged: (value) => currentPage.value = value,
                children: [
                  OnBoardingContent(
                    title: 'Track subscription',
                    subtitle: 'Track and manage all your '
                        'subscriptions easily from a single place',
                    image: Assets.images.onboarding1.image(),
                  ),
                  OnBoardingContent(
                    title: 'Reminders',
                    subtitle: 'Schedule notifications for your subscriptions '
                        'renewal at any given time',
                    image: Assets.images.onboarding2.image(),
                  ),
                  OnBoardingContent(
                    title: 'Statistics',
                    subtitle: 'Always care of your spending with the right '
                        'data and insightful charts',
                    image: Assets.images.onboarding3.image(),
                  ),
                ],
              ),
            ),
            // OnboardingNavigation()
            const Gap(101),
            SANavigation(
              itemCount: 3,
              selectedItem: currentPage.value,
              onTap: (value) => pageController.animateToPage(
                value,
                curve: Curves.ease,
                duration: _pageSwitchingDuration,
              ),
            ),
            const Gap(31),
            SAButton(
              text: currentPage.value != 2 ? 'Next' : 'Get started',
              onTap: () {
                if (currentPage.value != 2) {
                  pageController.nextPage(
                    curve: Curves.ease,
                    duration: _pageSwitchingDuration,
                  );
                } else {
                  QuizRoute().go(context);
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
