// ignore_for_file: lines_longer_than_80_chars

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:subscribe_app/application/subscription/subscription_bloc.dart';
import 'package:subscribe_app/domain/subscription/subscription.dart';
import 'package:subscribe_app/presentation/core/colours.dart';
import 'package:subscribe_app/presentation/core/common_widgets/sa_scaffold_widget.dart';
import 'package:subscribe_app/presentation/core/common_widgets/sa_subscribe_card_widget.dart';
import 'package:subscribe_app/presentation/core/helpers.dart';
import 'package:subscribe_app/presentation/core/text_styles.dart';
import 'package:subscribe_app/presentation/subscription/add_subscription/add_subscription_page.dart';

class CalendarPage extends HookWidget {
  const CalendarPage({super.key});

  static const route = '/calendar';

  @override
  Widget build(BuildContext context) {
    final calendarSelectedDate = useState(DateTime.now());

    return BlocBuilder<SubscriptionBloc, SubscriptionState>(
      builder: (context, state) {
        final selectedEvents = <DateTime, List<Event>>{};

        for (final subscription in state.subscriptions.asList()) {
          final expirationDate = subscription.expirationDate.getOrCrash();
          final event = Event(
            title:
                '${subscription.name.getOrCrash()} expiration date ${expirationDate.toFormattedDate()}',
            iconPath: subscription.iconPath,
          );

          if (selectedEvents.containsKey(expirationDate)) {
            selectedEvents[expirationDate]!.add(event);
          } else {
            selectedEvents[expirationDate] = [event];
          }
        }

        final sortedSubscriptions =
            List<Subscription>.from(state.subscriptions.asList())
              ..sort(
                (a, b) => a.expirationDate
                    .getOrCrash()
                    .compareTo(b.expirationDate.getOrCrash()),
              );

        return SAScaffold(
          body: Padding(
            padding: EdgeInsets.only(right: 25.r, left: 25.r, top: 32.r),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Calendar',
                  style: TextStyles.sfProText32SemiboldWhite,
                ),
                Gap(23.r),
                Container(
                  padding: EdgeInsets.all(25.r),
                  decoration: BoxDecoration(
                    color: Colours.grey2,
                    borderRadius: BorderRadius.circular(12.r),
                  ),
                  child: CustomCalendar(
                    onChanged: (value) {
                      calendarSelectedDate.value = value;
                    },
                    selectedEvents: selectedEvents,
                    selectedDate: calendarSelectedDate.value,
                  ),
                ),
                Gap(17.r),
                Text(
                  'Next payment',
                  style: TextStyles.sfProText24SemiboldWhite,
                ),
                Expanded(
                  child: ListView.separated(
                    padding: EdgeInsets.only(top: 17.r),
                    itemBuilder: (context, index) => SASubscribeCard(
                      subscription: sortedSubscriptions[index],
                      type: CardType.calendar,
                    ),
                    separatorBuilder: (context, index) => Gap(15.r),
                    itemCount: state.subscriptions.size,
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
