import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:subscribe_app/application/settings/settings_bloc.dart';
import 'package:subscribe_app/injection.dart';
import 'package:subscribe_app/presentation/core/colours.dart';
import 'package:subscribe_app/presentation/core/common_widgets/sa_scaffold_widget.dart';
import 'package:subscribe_app/presentation/core/text_styles.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({super.key});

  static const route = '/settings';

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<SettingsBloc>(),
      child: BlocBuilder<SettingsBloc, SettingsState>(
        builder: (context, state) {
          return SAScaffold(
            body: Padding(
              padding: EdgeInsets.only(right: 25.r, left: 25.r, top: 35.r),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Settings',
                    style: TextStyles.sfProText32SemiboldWhite,
                  ),
                  Gap(30.r),
                  SettingsItemCard(
                    title: 'Notifications',
                    switchable: true,
                    switchStatus: state.enabled,
                    tappable: false,
                  ),
                  const Gap(20),
                  SettingsItemCard(title: 'Support', titleColor: Colours.blue2),
                  const Gap(20),
                  SettingsItemCard(
                    title: 'Share app',
                    titleColor: Colours.blue2,
                  ),
                  const Gap(20),
                  SettingsItemCard(title: 'Rate us', titleColor: Colours.blue2),
                  const Gap(20),
                  SettingsItemCard(
                    title: 'Delete account',
                    titleColor: Colours.red,
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

class SettingsItemCard extends StatelessWidget {
  const SettingsItemCard({
    required this.title,
    this.switchable = false,
    this.switchStatus = false,
    this.tappable = true,
    this.titleColor,
    super.key,
  });

  final String title;
  final bool switchable;
  final bool switchStatus;
  final bool tappable;
  final Color? titleColor;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: tappable ? () {} : null,
      child: Container(
        width: 1.sw,
        padding: EdgeInsets.only(top: 10.r, bottom: 10.r, right: 10.r),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              width: 1.r,
              color: Colours.white,
            ),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: TextStyles.sfProText20RegularWhite
                  .copyWith(color: titleColor ?? Colours.white),
            ),
            if (switchable)
              CupertinoSwitch(
                value: switchStatus,
                onChanged: (value) {
                  context
                      .read<SettingsBloc>()
                      .add(SettingsEvent.statusChanged(status: value));
                },
              ),
          ],
        ),
      ),
    );
  }
}
