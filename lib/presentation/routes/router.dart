import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:subscribe_app/presentation/calendar/calendar_page.dart';
import 'package:subscribe_app/presentation/notification/notification_page.dart';
import 'package:subscribe_app/presentation/onboarding/onboarding_page.dart';
import 'package:subscribe_app/presentation/personafication/personafication_page.dart';
import 'package:subscribe_app/presentation/quiz/quiz_page.dart';
import 'package:subscribe_app/presentation/settings/settings_page.dart';
import 'package:subscribe_app/presentation/shell/shell_page.dart';
import 'package:subscribe_app/presentation/subscription/add_subscription/add_subscription_page.dart';
import 'package:subscribe_app/presentation/subscription/home_page.dart';

part 'shell_routes.dart';

part 'router.g.dart';

final router =
    GoRouter(routes: $appRoutes, initialLocation: HomePage.route);

@TypedGoRoute<AddSubscriptionRoute>(
  path: AddSubscriptionPage.route,
)
class AddSubscriptionRoute extends GoRouteData {
  @override
  Widget build(BuildContext context, GoRouterState state) =>
      const AddSubscriptionPage();
}

@TypedGoRoute<PersonaficationRoute>(
  path: PersonaficationPage.route,
)
class PersonaficationRoute extends GoRouteData {
  @override
  Widget build(BuildContext context, GoRouterState state) =>
      const PersonaficationPage();
}

@TypedGoRoute<QuizRoute>(
  path: QuizPage.route,
)
class QuizRoute extends GoRouteData {
  @override
  Widget build(BuildContext context, GoRouterState state) => const QuizPage();
}

@TypedGoRoute<OnBoardingRoute>(
  path: OnBoardingPage.route,
)
class OnBoardingRoute extends GoRouteData {
  @override
  Widget build(BuildContext context, GoRouterState state) =>
      const OnBoardingPage();
}
