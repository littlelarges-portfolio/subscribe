part of 'router.dart';

@TypedStatefulShellRoute<ShellRoute>(
  branches: <TypedStatefulShellBranch<StatefulShellBranchData>>[
    TypedStatefulShellBranch<HomeBranchRoute>(
      routes: <TypedRoute<RouteData>>[
        TypedGoRoute<HomeRoute>(path: HomePage.route),
      ],
    ),
    TypedStatefulShellBranch<CalendarBranchRoute>(
      routes: <TypedRoute<RouteData>>[
        TypedGoRoute<CalendarRoute>(path: CalendarPage.route),
      ],
    ),
    TypedStatefulShellBranch<NotificationBranchRoute>(
      routes: <TypedRoute<RouteData>>[
        TypedGoRoute<NotificationRoute>(path: NotificationPage.route),
      ],
    ),
    TypedStatefulShellBranch<SettingsBranchRoute>(
      routes: <TypedRoute<RouteData>>[
        TypedGoRoute<SettingsRoute>(path: SettingsPage.route),
      ],
    ),
  ],
)
class ShellRoute extends StatefulShellRouteData {
  const ShellRoute();

  @override
  Widget builder(
    BuildContext context,
    GoRouterState state,
    StatefulNavigationShell navigationShell,
  ) {
    return navigationShell;
  }

  static const String $restorationScopeId = 'restorationScopeId';

  static Widget $navigatorContainerBuilder(
    BuildContext context,
    StatefulNavigationShell navigationShell,
    List<Widget> children,
  ) {
    return ShellPage(
      navigationShell: navigationShell,
      children: children,
    );
  }
}

class HomeBranchRoute extends StatefulShellBranchData {
  const HomeBranchRoute();
}

class CalendarBranchRoute extends StatefulShellBranchData {
  const CalendarBranchRoute();
}

class NotificationBranchRoute extends StatefulShellBranchData {
  const NotificationBranchRoute();
}

class SettingsBranchRoute extends StatefulShellBranchData {
  const SettingsBranchRoute();
}

class HomeRoute extends GoRouteData {
  const HomeRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) => const HomePage();
}

class CalendarRoute extends GoRouteData {
  const CalendarRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) =>
      const CalendarPage();
}

class NotificationRoute extends GoRouteData {
  const NotificationRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) =>
      const NotificationPage();
}

class SettingsRoute extends GoRouteData {
  const SettingsRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) =>
      const SettingsPage();
}
