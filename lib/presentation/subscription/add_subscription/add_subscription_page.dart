// ignore_for_file: lines_longer_than_80_chars

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gap/gap.dart';
import 'package:intl/intl.dart';
import 'package:kt_dart/kt.dart';
import 'package:subscribe_app/application/subscription/add_subscription_form/add_subscription_form_bloc.dart';
import 'package:subscribe_app/application/subscription/subscription_bloc.dart';
import 'package:subscribe_app/injection.dart';
import 'package:subscribe_app/presentation/core/colours.dart';
import 'package:subscribe_app/presentation/core/common_widgets/sa_button_widget.dart';
import 'package:subscribe_app/presentation/core/common_widgets/sa_scaffold_widget.dart';
import 'package:subscribe_app/presentation/core/gen/assets.gen.dart';
import 'package:subscribe_app/presentation/core/helpers.dart';
import 'package:subscribe_app/presentation/core/text_styles.dart';
import 'package:table_calendar/table_calendar.dart';

class AddSubscriptionPage extends HookWidget {
  const AddSubscriptionPage({super.key});

  static const route = '/add-subscription';

  KtList<SvgGenImage> get _platformIcons => KtList.from([
        Assets.icons.youtube,
        Assets.icons.spotify,
        Assets.icons.appleMusic,
      ]);

  @override
  Widget build(BuildContext context) {
    final emptySubscriptionSelectedDate = useState(DateTime(0));
    final emptyExpirationSelectedDate = useState(DateTime(0));

    final subscriptionSelectedDate = useState(DateTime.now());
    final expirationSelectedDate = useState(DateTime.now());

    final subscriptionController = useTextEditingController();
    final expirationController = useTextEditingController();

    final subscriptionIconWidget =
        useState(Assets.icons.addSubscriptionIcon.svg());

    void openDatePicker({
      required BuildContext context,
      required DateTime selectedDate,
      required ValueChanged<DateTime> onDateSelected,
    }) {
      showDialog<Widget>(
        context: context,
        builder: (context) => AlertDialog(
          backgroundColor: Colours.grey2,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12.r)),
          ),
          content: CustomCalendar(
            onChanged: (value) => onDateSelected(value),
            selectedDate: selectedDate,
            readOnly: false,
          ),
        ),
      );
    }

    return BlocProvider(
      create: (context) => getIt<AddSubscriptionFormBloc>(),
      child: BlocConsumer<AddSubscriptionFormBloc, AddSubscriptionFormState>(
        listener: (context, state) {
          context
              .read<SubscriptionBloc>()
              .add(SubscriptionEvent.added(state.validatedSubscription!));

          Navigator.pop(context);
        },
        listenWhen: (previous, current) =>
            previous.validatedSubscription != current.validatedSubscription,
        builder: (context, state) {
          return SAScaffold(
            body: LayoutBuilder(
              builder: (BuildContext context, BoxConstraints constraints) =>
                  SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: SizedBox(
                  height: constraints.maxHeight,
                  width: 1.sw,
                  child: Stack(
                    children: [
                      Positioned(
                        top: 10.r,
                        left: 10.r,
                        child: IconButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          icon: Assets.icons.back.svg(),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          top: 22.r,
                        ),
                        child: Column(
                          children: [
                            IconButton(
                              padding: EdgeInsets.zero,
                              onPressed: () {
                                showModalBottomSheet<Widget>(
                                  isScrollControlled: true,
                                  context: context,
                                  enableDrag: false,
                                  builder: (modalContext) => BlocProvider.value(
                                    value: BlocProvider.of<
                                        AddSubscriptionFormBloc>(
                                      context,
                                    ),
                                    child: Wrap(
                                      children: [
                                        Center(
                                          child: SizedBox(
                                            height: 200.r,
                                            child: ListView.separated(
                                              shrinkWrap: true,
                                              padding: EdgeInsets.symmetric(
                                                horizontal: 20.r,
                                              ),
                                              scrollDirection: Axis.horizontal,
                                              itemBuilder: (context, index) =>
                                                  SizedBox(
                                                height: 70.r,
                                                width: 70.r,
                                                child: Center(
                                                  child: IconButton(
                                                    onPressed: () {
                                                      context
                                                          .read<
                                                              AddSubscriptionFormBloc>()
                                                          .add(
                                                            AddSubscriptionFormEvent
                                                                .iconPathChanged(
                                                              _platformIcons[
                                                                      index]
                                                                  .path,
                                                            ),
                                                          );
                                                      subscriptionIconWidget
                                                              .value =
                                                          _platformIcons[index]
                                                              .svg(
                                                        height: 110.r,
                                                      );

                                                      Navigator.pop(context);
                                                    },
                                                    icon: _platformIcons[index]
                                                        .svg(height: 60.r),
                                                  ),
                                                ),
                                              ),
                                              separatorBuilder:
                                                  (context, index) => Gap(20.r),
                                              itemCount: 3,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                              icon: subscriptionIconWidget.value,
                            ),
                            Gap(4.r),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                horizontal: 25.r,
                              ),
                              child: Form(
                                autovalidateMode: state.showErrorMessages,
                                child: Column(
                                  children: [
                                    ConstrainedBox(
                                      constraints: BoxConstraints(
                                        minWidth: 90.r,
                                        maxWidth: 200.r,
                                      ),
                                      child: IntrinsicWidth(
                                        child: TextFormField(
                                          validator: (_) => context
                                              .read<AddSubscriptionFormBloc>()
                                              .state
                                              .name
                                              .value
                                              .fold(
                                                (f) => f.maybeWhen(
                                                  subscription:
                                                      (subscriptionFailiure) =>
                                                          subscriptionFailiure
                                                              .maybeMap(
                                                    cantBeEmpty: (_) =>
                                                        "Can't be empty",
                                                    orElse: () => null,
                                                  ),
                                                  orElse: () => null,
                                                ),
                                                (r) => null,
                                              ),
                                          onChanged: (value) => context
                                              .read<AddSubscriptionFormBloc>()
                                              .add(
                                                AddSubscriptionFormEvent
                                                    .nameChanged(
                                                  value,
                                                ),
                                              ),
                                          decoration: InputDecoration(
                                            hintText: 'Name',
                                            hintStyle: TextStyles
                                                .sfProText32SemiboldGrey,
                                          ),
                                          style: TextStyles
                                              .sfProText32SemiboldWhite,
                                        ),
                                      ),
                                    ),
                                    Gap(40.r),
                                    TextFormField(
                                      validator: (_) => context
                                          .read<AddSubscriptionFormBloc>()
                                          .state
                                          .cost
                                          .value
                                          .fold(
                                            (f) => f.maybeWhen(
                                              subscription:
                                                  (subscriptionFailiure) =>
                                                      subscriptionFailiure
                                                          .maybeMap(
                                                invalidCost: (_) =>
                                                    'Invalid cost',
                                                orElse: () => null,
                                              ),
                                              orElse: () => null,
                                            ),
                                            (r) => null,
                                          ),
                                      onChanged: (value) => context
                                          .read<AddSubscriptionFormBloc>()
                                          .add(
                                            AddSubscriptionFormEvent
                                                .costChanged(
                                              value,
                                            ),
                                          ),
                                      decoration: InputDecoration(
                                        hintText: 'Cost',
                                        hintStyle:
                                            TextStyles.sfProText16SemiboldGrey,
                                      ),
                                      style:
                                          TextStyles.sfProText16SemiboldWhite,
                                    ),
                                    Gap(40.r),
                                    TextFormField(
                                      validator: (_) => context
                                          .read<AddSubscriptionFormBloc>()
                                          .state
                                          .siteUrl
                                          .value
                                          .fold(
                                            (f) => f.maybeWhen(
                                              subscription:
                                                  (subscriptionFailiure) =>
                                                      subscriptionFailiure
                                                          .maybeMap(
                                                cantBeEmpty: (_) =>
                                                    "Can't be empty",
                                                orElse: () => null,
                                              ),
                                              orElse: () => null,
                                            ),
                                            (r) => null,
                                          ),
                                      onChanged: (value) => context
                                          .read<AddSubscriptionFormBloc>()
                                          .add(
                                            AddSubscriptionFormEvent
                                                .siteUrlChanged(
                                              value,
                                            ),
                                          ),
                                      decoration: InputDecoration(
                                        hintText: 'Site URL',
                                        hintStyle:
                                            TextStyles.sfProText16SemiboldGrey,
                                      ),
                                      style:
                                          TextStyles.sfProText16SemiboldWhite,
                                    ),
                                    Gap(40.r),
                                    TextFormField(
                                      onChanged: (value) => context
                                          .read<AddSubscriptionFormBloc>()
                                          .add(
                                            AddSubscriptionFormEvent
                                                .noteChanged(
                                              value,
                                            ),
                                          ),
                                      decoration: InputDecoration(
                                        hintText: 'Note',
                                        hintStyle:
                                            TextStyles.sfProText16SemiboldGrey,
                                      ),
                                      style:
                                          TextStyles.sfProText16SemiboldWhite,
                                    ),
                                    Gap(40.r),
                                    TextFormField(
                                      controller: subscriptionController,
                                      decoration: InputDecoration(
                                        hintText: 'Subscription date',
                                        hintStyle:
                                            TextStyles.sfProText16SemiboldGrey,
                                      ),
                                      style:
                                          TextStyles.sfProText16SemiboldWhite,
                                      readOnly: true,
                                      onTap: () {
                                        openDatePicker(
                                          context: context,
                                          selectedDate:
                                              subscriptionSelectedDate.value,
                                          onDateSelected: (value) {
                                            emptySubscriptionSelectedDate
                                                .value = value;

                                            subscriptionSelectedDate.value =
                                                value;
                                            subscriptionController.text =
                                                DateFormat('dd-MM-yyyy')
                                                    .format(value)
                                                    .replaceAll('-', '.')
                                                    .split(' ')[0];

                                            context
                                                .read<AddSubscriptionFormBloc>()
                                                .add(
                                                  AddSubscriptionFormEvent
                                                      .subscriptionDateChanged(
                                                    subscriptionDate:
                                                        value.normalize(),
                                                    expirationDate:
                                                        emptyExpirationSelectedDate
                                                            .value
                                                            .normalize(),
                                                  ),
                                                );
                                          },
                                        );
                                      },
                                      validator: (value) => context
                                          .read<AddSubscriptionFormBloc>()
                                          .state
                                          .subscriptionDate
                                          .value
                                          .fold(
                                            (f) => f.maybeWhen(
                                              subscription:
                                                  (subscriptionFailiure) =>
                                                      subscriptionFailiure
                                                          .maybeMap(
                                                cantBeEmpty: (_) =>
                                                    "Can't be empty",
                                                subscriptionDateCantBeAfterExpirationDate:
                                                    (_) =>
                                                        "Subscription date can't be after the expiration date.",
                                                orElse: () => null,
                                              ),
                                              orElse: () => null,
                                            ),
                                            (r) => null,
                                          ),
                                    ),
                                    Gap(40.r),
                                    TextFormField(
                                      controller: expirationController,
                                      decoration: InputDecoration(
                                        hintText: 'Expiration date',
                                        hintStyle:
                                            TextStyles.sfProText16SemiboldGrey,
                                      ),
                                      style:
                                          TextStyles.sfProText16SemiboldWhite,
                                      readOnly: true,
                                      onTap: () {
                                        openDatePicker(
                                          context: context,
                                          selectedDate:
                                              expirationSelectedDate.value,
                                          onDateSelected: (value) {
                                            emptyExpirationSelectedDate.value =
                                                value;

                                            expirationSelectedDate.value =
                                                value;
                                            expirationController.text =
                                                value.toFormattedDate();

                                            context
                                                .read<AddSubscriptionFormBloc>()
                                                .add(
                                                  AddSubscriptionFormEvent
                                                      .expirationDateChanged(
                                                    expirationDate:
                                                        value.normalize(),
                                                    subscriptionDate:
                                                        emptySubscriptionSelectedDate
                                                            .value
                                                            .normalize(),
                                                  ),
                                                );
                                          },
                                        );
                                      },
                                      validator: (value) => context
                                          .read<AddSubscriptionFormBloc>()
                                          .state
                                          .expirationDate
                                          .value
                                          .fold(
                                            (f) => f.maybeWhen(
                                              subscription:
                                                  (subscriptionFailiure) =>
                                                      subscriptionFailiure
                                                          .maybeMap(
                                                cantBeEmpty: (_) =>
                                                    "Can't be empty",
                                                expirationDateCantBeBeforeSubscriptionDate:
                                                    (_) =>
                                                        "Expiration date can't be before the subscription date.",
                                                orElse: () => null,
                                              ),
                                              orElse: () => null,
                                            ),
                                            (r) => null,
                                          ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            const Spacer(),
                            SAButton(
                              text: 'Send me notifications',
                              onTap: () {
                                context.read<AddSubscriptionFormBloc>().add(
                                      const AddSubscriptionFormEvent
                                          .sendMeNotificationsPressed(),
                                    );
                              },
                              rounded: false,
                            ),
                            const Spacer(),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}

class CustomCalendar extends StatelessWidget {
  const CustomCalendar({
    required this.onChanged,
    required this.selectedDate,
    this.readOnly = true,
    this.selectedEvents,
    super.key,
  });

  final ValueChanged<DateTime> onChanged;
  final DateTime selectedDate;
  final bool readOnly;

  double get _calendarCellBorderRadius => 3.4.r;

  final Map<DateTime, List<Event>>? selectedEvents;

  List<Event> _getEventsForDay(DateTime day) {
    final events = selectedEvents?[day.normalize()];

    return events ?? [];
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 344.r,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TableCalendar<dynamic>(
            startingDayOfWeek: StartingDayOfWeek.monday,
            weekendDays: const [],
            headerVisible: false,
            firstDay: DateTime.utc(2020),
            lastDay: DateTime.utc(2030, 12, 31),
            focusedDay: selectedDate,
            eventLoader: _getEventsForDay,
            selectedDayPredicate: (day) {
              return isSameDay(selectedDate, day);
            },
            onDaySelected: (selectedDay, focusedDay) {
              final events = _getEventsForDay(selectedDay);
              
              if (readOnly) {
                onChanged(selectedDay);
                showDialog<Widget>(
                  context: context,
                  builder: (context) => AlertDialog(
                    backgroundColor: Colours.grey2,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12.r)),
                    ),
                    content: SizedBox(
                      height: 300.r,
                      width: 300.r,
                      child: ListView(
                        shrinkWrap: true,
                        children: events.map((event) {
                          return Column(
                            children: [
                              Gap(30.r),
                              Column(
                                children: [
                                  if (event.iconPath != null)
                                    SvgPicture.asset(
                                      event.iconPath!,
                                      height: 70.r,
                                    )
                                  else
                                    Container(),
                                  Gap(30.r),
                                  Text(
                                    event.title,
                                    style:
                                        TextStyles.sfProDisplay20SemiboldWhite,
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                            ],
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                );
              } else {
                onChanged(selectedDay);

                Navigator.pop(context);
              }
            },
            daysOfWeekStyle: DaysOfWeekStyle(
              weekdayStyle: TextStyles.inter14RegularWhite,
              dowTextFormatter: (date, locale) {
                if (date.weekday == 1) {
                  return DateFormat.E(locale).format(date).substring(0, 3);
                } else {
                  return DateFormat.E(locale).format(date).substring(0, 2);
                }
              },
            ),
            calendarStyle: const CalendarStyle(
              markerDecoration: BoxDecoration(
                color: Colors.red,
              ),
            ),
            calendarBuilders: CalendarBuilders(
              defaultBuilder: (context, day, focusedDay) {
                return Cell(
                  text: '${day.day}',
                  backgroundColor: Colours.white,
                  calendarCellBorderRadius: _calendarCellBorderRadius,
                );
              },
              todayBuilder: (context, day, focusedDay) {
                return Cell(
                  text: '${day.day}',
                  backgroundColor: Colours.white,
                  calendarCellBorderRadius: _calendarCellBorderRadius,
                );
              },
              selectedBuilder: (context, day, focusedDay) {
                return Cell(
                  text: '${day.day}',
                  backgroundColor: Colours.blue3,
                  calendarCellBorderRadius: _calendarCellBorderRadius,
                );
              },
              outsideBuilder: (context, day, focusedDay) {
                return Cell(
                  text: '${day.day}',
                  backgroundColor: Colours.grey3,
                  calendarCellBorderRadius: _calendarCellBorderRadius,
                  textColor: Colours.grey4,
                );
              },
              markerBuilder: (context, day, events) {
                if (events.isNotEmpty) {
                  return Cell(
                    text: '${day.day}',
                    backgroundColor: Colours.blue3,
                    calendarCellBorderRadius: _calendarCellBorderRadius,
                    textColor: Colours.black,
                  );
                }
                return null;
              },
            ),
          ),
        ],
      ),
    );
  }
}

class Event {
  const Event({required this.title, this.iconPath});

  final String title;
  final String? iconPath;

  @override
  String toString() => title;
}

class Cell extends StatelessWidget {
  const Cell({
    required this.text,
    required this.backgroundColor,
    required this.calendarCellBorderRadius,
    this.textColor,
    super.key,
  });

  final String text;
  final Color backgroundColor;
  final double calendarCellBorderRadius;
  final Color? textColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 38.r,
      height: 38.r,
      margin: EdgeInsets.zero,
      padding: EdgeInsets.zero,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: BorderRadius.circular(calendarCellBorderRadius),
      ),
      child: Text(
        text,
        style: textColor != null
            ? TextStyles.inter17RegularBlack.copyWith(color: textColor)
            : TextStyles.inter17RegularBlack,
      ),
    );
  }
}
