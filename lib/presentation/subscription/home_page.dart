// ignore_for_file: lines_longer_than_80_chars

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gap/gap.dart';
import 'package:kt_dart/kt.dart';
import 'package:subscribe_app/application/subscription/subscription_bloc.dart';
import 'package:subscribe_app/presentation/core/colours.dart';
import 'package:subscribe_app/presentation/core/common_widgets/sa_scaffold_widget.dart';
import 'package:subscribe_app/presentation/core/common_widgets/sa_subscribe_card_widget.dart';
import 'package:subscribe_app/presentation/core/gen/assets.gen.dart';
import 'package:subscribe_app/presentation/core/text_styles.dart';
import 'package:subscribe_app/presentation/routes/router.dart';
import 'package:subscribe_app/presentation/subscription/widgets/custom_circle_progress_bar_indicator.dart';

class HomePage extends HookWidget {
  const HomePage({super.key});

  static const route = '/home';

  double get _tabBarBorderRadius => 8.r;

  double calculateDateProgress(
    DateTime startDate,
    DateTime endDate,
  ) {
    final currentDate = DateTime.now();

    if (currentDate.isBefore(startDate)) {
      return 0;
    }
    if (currentDate.isAfter(endDate)) {
      return 1;
    }

    final totalDuration = endDate.difference(startDate).inDays;
    final currentDuration = currentDate.difference(startDate).inDays;

    return currentDuration / totalDuration;
  }

  @override
  Widget build(BuildContext context) {
    final tabController = useTabController(initialLength: 2);

    return BlocBuilder<SubscriptionBloc, SubscriptionState>(
      builder: (context, state) {
        return SAScaffold(
          body: SizedBox(
            width: 1.sw,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(
                    right: 25.r,
                    left: 25.r,
                    top: 25.r,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Subscription',
                            style: TextStyles.sfProText32SemiboldWhite,
                          ),
                          Gap(8.r),
                          Text.rich(
                            TextSpan(
                              text: 'Per month',
                              style: TextStyles.sfProText16MediumWhite,
                              children: [
                                TextSpan(
                                  text:
                                      ' ${state.subscriptions.map((subscription) => int.parse(subscription.cost.getOrCrash())).fold(0, (prev, item) => prev + item)}\$',
                                  style: TextStyles.sfProText16MediumGrey,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      IconButton(
                        onPressed: () {
                          AddSubscriptionRoute()
                              .push<AddSubscriptionRoute>(context);
                        },
                        icon: Assets.icons.addSubscription.svg(),
                      ),
                    ],
                  ),
                ),
                Gap(36.r),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: 25.r),
                        child: Text(
                          'Reminder',
                          style: TextStyles.sfProText20SemiboldWhite,
                        ),
                      ),
                      Gap(15.r),
                      SizedBox(
                        height: 100.r,
                        child: ListView.separated(
                          padding: EdgeInsets.symmetric(horizontal: 25.r),
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) => Container(
                            height: 100.r,
                            width: 117.r,
                            decoration: BoxDecoration(
                              color: Colours.lightBlueWithOpacity08,
                              borderRadius: BorderRadius.circular(12.r),
                              boxShadow: [
                                BoxShadow(
                                  color: Colours.blackWithOpacity025,
                                  blurRadius: 4,
                                  offset: const Offset(0, 4),
                                ),
                              ],
                            ),
                            child: Column(
                              children: [
                                const Spacer(),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Spacer(),
                                    const Spacer(),
                                    if (state.subscriptions[index].iconPath !=
                                        null)
                                      SvgPicture.asset(
                                        state.subscriptions[index].iconPath!,
                                      )
                                    else
                                      Container(),
                                    const Spacer(),
                                    Column(
                                      children: [
                                        Text(
                                          state.subscriptions[index].name
                                              .getOrCrash(),
                                          style:
                                              TextStyles.sfProText14MediumBlack,
                                        ),
                                        Text(
                                          '${state.subscriptions[index].cost.getOrCrash()}\$',
                                          style:
                                              TextStyles.sfProText11MediumGrey,
                                        ),
                                      ],
                                    ),
                                    const Spacer(),
                                    const Spacer(),
                                  ],
                                ),
                                Gap(10.r),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Spacer(),
                                    const Spacer(),
                                    Stack(
                                      children: [
                                        Positioned.fill(
                                          child: Center(
                                            child: Text(
                                              '${state.subscriptions[index].expirationDate.getOrCrash().difference(DateTime.now()).inDays + 1}',
                                              style: TextStyles
                                                  .sfProText14MediumBlack,
                                            ),
                                          ),
                                        ),
                                        CustomCircleProgressBarIndicator(
                                          value: calculateDateProgress(
                                            state.subscriptions[index]
                                                .subscriptionDate
                                                .getOrCrash(),
                                            state.subscriptions[index]
                                                .expirationDate
                                                .getOrCrash(),
                                          ),
                                        ),
                                      ],
                                    ),
                                    const Spacer(),
                                    SizedBox(
                                      width: 52.r,
                                      child: Text(
                                        'Days remaining',
                                        style:
                                            TextStyles.sfProText11MediumBlack,
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    const Spacer(),
                                    const Spacer(),
                                  ],
                                ),
                                const Spacer(),
                              ],
                            ),
                          ),
                          separatorBuilder: (context, index) => Gap(15.r),
                          itemCount: state.subscriptions.size,
                        ),
                      ),
                      Gap(30.r),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 25.r),
                          child: Column(
                            children: [
                              Container(
                                height: 32.r,
                                padding: EdgeInsets.all(2.r),
                                decoration: BoxDecoration(
                                  color: Colours.grey7,
                                  borderRadius: BorderRadius.circular(
                                    _tabBarBorderRadius,
                                  ),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colours.blackWithOpacity025,
                                      blurRadius: 4,
                                      offset: const Offset(0, 4),
                                    ),
                                  ],
                                ),
                                child: TabBar(
                                  controller: tabController,
                                  dividerColor: Colors.transparent,
                                  overlayColor: const WidgetStatePropertyAll(
                                    Colors.transparent,
                                  ),
                                  indicatorColor: Colors.transparent,
                                  unselectedLabelColor: Colours.grey8,
                                  indicatorSize: TabBarIndicatorSize.tab,
                                  indicator: BoxDecoration(
                                    color: Colours.white,
                                    borderRadius: BorderRadius.circular(
                                      _tabBarBorderRadius,
                                    ),
                                  ),
                                  labelColor: Colours.black,
                                  tabs: const [
                                    Tab(
                                      text: 'Active',
                                    ),
                                    Tab(
                                      text: 'Inactive',
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: TabBarView(
                                  controller: tabController,
                                  children: [
                                    ListView.separated(
                                      padding: EdgeInsets.only(top: 20.r),
                                      itemBuilder: (context, index) =>
                                          SASubscribeCard(
                                        subscription:
                                            state.subscriptions[index],
                                        type: CardType.active,
                                      ),
                                      separatorBuilder: (context, index) =>
                                          Gap(15.r),
                                      itemCount: state.subscriptions.size,
                                    ),
                                    const Center(
                                      child: Text(
                                        'Inactive',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
