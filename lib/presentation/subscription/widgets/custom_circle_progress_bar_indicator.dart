import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:subscribe_app/presentation/core/colours.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';

class CustomCircleProgressBarIndicator extends StatelessWidget {
  const CustomCircleProgressBarIndicator({
    this.radius,
    this.value,
    super.key,
  });

  final double? radius;
  final double? value;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: radius ?? 32.r,
      width: radius ?? 32.r,
      child: SfRadialGauge(
        axes: <RadialAxis>[
          RadialAxis(
            maximum: 1,
            startAngle: 90,
            endAngle: 90,
            showLabels: false,
            showTicks: false,
            axisLineStyle: AxisLineStyle(
              thickness: 0.3,
              color: Colours.white,
              thicknessUnit: GaugeSizeUnit.factor,
            ),
            pointers: <GaugePointer>[
              RangePointer(
                color: Colours.minks,
                value: value ?? 0,
                cornerStyle: CornerStyle.bothCurve,
                width: 0.3,
                sizeUnit: GaugeSizeUnit.factor,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
