# Subscription Tracker App 

[![style: very good analysis](https://img.shields.io/badge/style-very_good_analysis-B22C89.svg)](https://pub.dev/packages/very_good_analysis)

## 🔍 Overview

 Subscription Tracker app helps you manage your subscription expenses effortlessly. Keep track of your various subscriptions, their expiration dates, and get notified before they renew. Say goodbye to unexpected charges and keep your finances in check! 💸

## ✨ Features

- **Add Platforms**: Easily add your subscription platforms like Netflix, Spotify, or Amazon Prime.
- **Track Subscriptions**: Specify subscription details such as the type and expiration date.
- **Notifications**: Receive timely notifications before your subscriptions renew.
- **Total Spend**: Keep an eye on your total subscription expenses.
- **Upcoming Subscriptions**: Get a glimpse of your upcoming subscriptions for better planning.
- **Flexible Notification Settings**: Customize notification preferences based on your preferences.

## :iphone: Screens

| Home                                              | Calendar                                                      | Notification                              | Settings                              | Onboarding                           | Quiz                            | Personafication                           |
|---------------------------------------------------|---------------------------------------------------------------|-------------------------------------------|---------------------------------------|--------------------------------------|---------------------------------|-------------------------------------------|
| ![](./readme_files/home_page.png)                 | ![](./readme_files/calendar_page.png)                         | ![](./readme_files/notification_page.png) | ![](./readme_files/settings_page.png) | ![](./readme_files/onboarding_1.png) | ![](./readme_files/quiz_1.png)  | ![](./readme_files/personafication_1.png) |
| Getting Started                                   | Subscription Expiration Date Details            |                                           |                                       | ![](./readme_files/onboarding_2.png) | ![](./readme_files/quiz_2.png)  | ![](./readme_files/personafication_2.png) |
| ![](./readme_files/home_page_getting_started.png) | ![](./readme_files/calendar_page_expiration_date_details.png) |                                           |                                       | ![](./readme_files/onboarding_3.png) | ![](./readme_files/quiz_3.png)  | ![](./readme_files/personafication_3.png) |
|                                                   |                                                               |                                           |                                       |                                      | ![](./readme_files/quiz_4.png)  | ![](./readme_files/personafication_4.png) |
|                                                   |                                                               |                                           |                                       |                                      | ![](./readme_files/quiz_5.png)  | ![](./readme_files/personafication_5.png) |
|                                                   |                                                               |                                           |                                       |                                      | ![](./readme_files/quiz_6.png)  |                                           |
|                                                   |                                                               |                                           |                                       |                                      | ![](./readme_files/quiz_7.png)  |                                           |
|                                                   |                                                               |                                           |                                       |                                      | ![](./readme_files/quiz_8.png)  |                                           |
|                                                   |                                                               |                                           |                                       |                                      | ![](./readme_files/quiz_9.png)  |                                           |
|                                                   |                                                               |                                           |                                       |                                      | ![](./readme_files/quiz_10.png) |                                           |

## 🔫 Getting Started

Follow these steps to get your development environment set up:

### Prerequisites

- Ensure you have Flutter installed on your machine. If you don't have Flutter installed, follow the instructions on the Flutter official website.


### Installation

1. Clone the repository to your local machine:

```
git clone https://gitlab.com/littlelarges-portfolio/subscribe.git
```
2. Navigate to the project directory:
```
cd subscribe
```

3. Get the required dependencies:
```
flutter pub get
```

### Running the Application

1. Ensure you are in the project root directory.

2. Execute the following command to run the application:

```
flutter run
```

## 🛠️ Tech Stack

<table>
    <tr>
        <td>Bloc</td>
        <td>Dartz</td>
        <td>Flutter Hooks</td>
        <td>Get It</td>
    </tr>
    <tr>
        <td>Freezed</td>
        <td>Equatable</td>
        <td>Go Router</td>
        <td>Injectable</td>
    </tr>
</table>

## 🏗️ Code Generation Stack

<table>
    <tr>
        <td>Go Router Builder</td>
        <td>Injectable Generator</td>
    </tr>
</table>

## 🤝 Contributing 

We welcome contributions! Feel free to fork this repository and submit pull requests with your improvements or bug fixes.

## 📝 Feedback 

Have any suggestions or issues? We'd love to hear from you! Please open an issue in this repository.


---

Enjoy tracking your subscriptions with ease! If you have any questions, feel free to reach out. Happy budgeting! 🎉📊